FROM openjdk

WORKDIR /

EXPOSE 8080

ADD ./build/libs/CoworkingSystem-0.0.1-SNAPSHOT.jar /

RUN sh -c 'touch CoworkingSystem-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java", "-jar", "CoworkingSystem-0.0.1-SNAPSHOT.jar"]