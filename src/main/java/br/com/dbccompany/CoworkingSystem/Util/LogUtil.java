package br.com.dbccompany.CoworkingSystem.Util;

import br.com.dbccompany.CoworkingSystem.CoworkingSystemApplication;
import br.com.dbccompany.CoworkingSystem.DTO.LogDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class LogUtil {
    private static Logger logger = LoggerFactory.getLogger(CoworkingSystemApplication.class);
    private static RestTemplate restTemplate = new RestTemplate();
    private static String url = "http://localhost:8081/api/logger/salvar";

    public static void enviarErro400(Exception e) {
        String mensagem = "Erro ao realizar a requisição!";
        System.out.println(e.getMessage());
        logger.error(mensagem);
        LogDTO log = new LogDTO("ERROR", "400", e, mensagem);
        restTemplate.postForObject(url, log, Object.class);
    }

    public static void enviarErro400(Exception e, String mensagem) {
        System.out.println(e.getMessage());
        logger.error(mensagem);
        LogDTO log = new LogDTO("ERROR", "400", e, mensagem);
        restTemplate.postForObject(url, log, Object.class);
    }

    public static void enviarErro404(Exception e) {
        String mensagem = "Erro ao realizar a requisição!";
        System.out.println(e.getMessage());
        logger.error(mensagem);
        LogDTO log = new LogDTO("ERROR", "404", e, mensagem);
        restTemplate.postForObject(url, log, Object.class);
    }

    public static void enviarErro404(Exception e, String mensagem) {
        System.out.println(e.getMessage());
        logger.error(mensagem);
        LogDTO log = new LogDTO("ERROR", "404", e, mensagem);
        restTemplate.postForObject(url, log, Object.class);
    }
}
