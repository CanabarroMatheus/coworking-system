package br.com.dbccompany.CoworkingSystem.Util;

public class MoedaUtil {

    public static String converter(Double valor) {
        StringBuilder valorFormatado = new StringBuilder();
        valorFormatado.append("R$ ");
        valorFormatado.append(valor);
        return valorFormatado.toString().replace(".", ",") + "0";
    }

    public static Double limparCurrencyString(String valor) {
        String valorFormatado = valor.replace("R$", "")
                .replace(".", "")
                .replace(" ", "")
                .replace(",", ".");
        return Double.parseDouble(valorFormatado);
    }
}
