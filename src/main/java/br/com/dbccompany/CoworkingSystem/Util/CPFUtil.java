package br.com.dbccompany.CoworkingSystem.Util;

public class CPFUtil {

    public static String converterCharParaString(Character[] cpf) {
        StringBuilder cpfEmString = new StringBuilder();
        for (Character digito : cpf) {
            cpfEmString.append(digito);
        }
        return cpfEmString.toString();
    }

    public static Character[] converterStringParaChar(String cpf) {
        Character[] cpfEmChar = new Character[11];
        for (int i = 0; i < cpf.length(); i++) {
            cpfEmChar[i] = cpf.charAt(i);
        }
        return cpfEmChar;
    }

}
