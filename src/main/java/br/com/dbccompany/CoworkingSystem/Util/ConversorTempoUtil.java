package br.com.dbccompany.CoworkingSystem.Util;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;

import java.time.Duration;

public class ConversorTempoUtil {

    public static Integer converterParaMinutos(Integer quantidade, TipoContratacaoEnum tipoContratacao) {
        switch (tipoContratacao) {
            case HORAS:
                return quantidade * 60;
            case TURNOS:
                return quantidade * 60 * 5;
            case DIARIAS:
                return quantidade * 60 * 5 * 2;
            case SEMANAS:
                return quantidade * 60 * 5 * 2 * 5;
            case MESES:
                return quantidade * 60 * 5 * 2 * 5 * 4;
            default:
                return quantidade;
        }
    }

    public static Integer diferencaEntreTempos(Integer tempoTotal, Duration tempoGasto, TipoContratacaoEnum tipoContratacao) {
        switch (tipoContratacao) {
            case HORAS:
                return tempoTotal - ((int) tempoGasto.toHours());
            case TURNOS:
                return tempoTotal - ((int) tempoGasto.toHours() / 5);
            case DIARIAS:
                return tempoTotal - ((int) tempoGasto.toDays());
            case SEMANAS:
                return tempoTotal - ((int) tempoGasto.toDays() / 5);
            case MESES:
                return tempoTotal - ((int) tempoGasto.toDays() / 30);
            default:
                return tempoTotal - ((int) tempoGasto.toMinutes());
        }
    }

}
