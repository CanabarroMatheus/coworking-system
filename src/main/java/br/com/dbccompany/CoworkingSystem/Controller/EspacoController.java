package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.EspacoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETEspacoDTO;
import br.com.dbccompany.CoworkingSystem.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {

    @Autowired
    private EspacoService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETEspacoDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETEspacoDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETEspacoDTO> salvar(@RequestBody EspacoDTO espacoDTO) {
        try {
            return new ResponseEntity<>(this.service.salvar(espacoDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public ResponseEntity<GETEspacoDTO> editar(@RequestBody EspacoDTO espacoDTO, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.editar(espacoDTO, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
