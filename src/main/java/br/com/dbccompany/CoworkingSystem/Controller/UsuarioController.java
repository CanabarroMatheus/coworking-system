package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETUsuarioDTO;
import br.com.dbccompany.CoworkingSystem.DTO.UsuarioDTO;
import br.com.dbccompany.CoworkingSystem.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @PostMapping("/salvar")
    @ResponseBody
    public GETUsuarioDTO salvar(@RequestBody UsuarioDTO usuarioDTO) {
        return this.service.salvarUsuario(usuarioDTO);
    }

}
