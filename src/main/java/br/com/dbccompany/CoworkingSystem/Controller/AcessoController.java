package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.AcessoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETAcessoDTO;
import br.com.dbccompany.CoworkingSystem.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    private AcessoService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETAcessoDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETAcessoDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/entrar")
    @ResponseBody
    public ResponseEntity<GETAcessoDTO> entrar(@RequestBody AcessoDTO acessoDTO) {
        try {
            return new ResponseEntity<>(this.service.entrar(acessoDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/entrar/especial")
    @ResponseBody
    public ResponseEntity<GETAcessoDTO> entradaEspecial(@RequestBody AcessoDTO acessoDTO) {
        try {
            return new ResponseEntity<>(this.service.entradaEspecial(acessoDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/sair")
    @ResponseBody
    public ResponseEntity<GETAcessoDTO> sair(@RequestBody AcessoDTO acessoDTO) {
        try {
            return new ResponseEntity<>(this.service.sair(acessoDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
