package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.EspacoPacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETEspacoPacoteDTO;
import br.com.dbccompany.CoworkingSystem.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacoPacote")
public class EspacoPacoteController {

    @Autowired
    private EspacoPacoteService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETEspacoPacoteDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETEspacoPacoteDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETEspacoPacoteDTO> salvar(@RequestBody EspacoPacoteDTO espacoPacoteDTO) {
        try {
            return new ResponseEntity<>(this.service.salvar(espacoPacoteDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
