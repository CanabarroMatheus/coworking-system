package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETPacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.PacoteDTO;
import br.com.dbccompany.CoworkingSystem.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {
    @Autowired
    private PacoteService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETPacoteDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETPacoteDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETPacoteDTO> salvar(@RequestBody PacoteDTO pacoteDTO) {
        try {
            return new ResponseEntity<>(this.service.salvar(pacoteDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public ResponseEntity<GETPacoteDTO> editar(@RequestBody PacoteDTO pacoteDTO, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.editar(pacoteDTO, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
