package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETTipoContatoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.TipoContatoDTO;
import br.com.dbccompany.CoworkingSystem.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETTipoContatoDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodosTiposDeContatos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/trazer/todos/nomes")
    @ResponseBody
    public ResponseEntity<List<GETTipoContatoDTO>> trazerTodosPorNomes(@RequestBody List<String> nomes) {
        try {
            return new ResponseEntity<>(this.service.trazerTodosTiposDeContatosPorNome(nomes), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETTipoContatoDTO> trazerPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/nome/{nome}")
    @ResponseBody
    public ResponseEntity<GETTipoContatoDTO> trazerPorNome(@PathVariable String nome) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorNome(nome), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETTipoContatoDTO> salvar(@RequestBody TipoContatoDTO tipoContato) {
        try {
            return new ResponseEntity<>(this.service.salvar(tipoContato), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public ResponseEntity<GETTipoContatoDTO> editar(@RequestBody TipoContatoDTO tipoContato, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.editar(tipoContato, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
