package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.ContratacaoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETContratacaoDTO;
import br.com.dbccompany.CoworkingSystem.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETContratacaoDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETContratacaoDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETContratacaoDTO> salvar(@RequestBody ContratacaoDTO contratacaoDTO) {
        try {
            return new ResponseEntity<>(this.service.salvar(contratacaoDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
