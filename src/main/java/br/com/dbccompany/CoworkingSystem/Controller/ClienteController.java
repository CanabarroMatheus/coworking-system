package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.ClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETClienteDTO;
import br.com.dbccompany.CoworkingSystem.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETClienteDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/trazer/todos/nome")
    @ResponseBody
    public ResponseEntity<List<GETClienteDTO>> trazerTodosPorNome(@RequestBody List<String> nomes) {
        try {
            return new ResponseEntity<>(this.service.trazerTodosPorNomes(nomes), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/todos/dataNascimento/{dataNascimento}")
    @ResponseBody
    public ResponseEntity<List<GETClienteDTO>> trazerTodosPorDataNascimento(@PathVariable Date dataNascimento) {
        try {
            return new ResponseEntity<>(this.service.trazerTodosPorDataNascimento(dataNascimento), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/trazer/todos/dataNascimento")
    @ResponseBody
    public ResponseEntity<List<GETClienteDTO>> trazerTodosPorDataNascimentoIn(@RequestBody List<Date> datasNascimento) {
        try {
            return new ResponseEntity<>(this.service.trazerTodosPorDataNascimentoIn(datasNascimento), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETClienteDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @GetMapping("/trazer/cpf/{cpf}")
    @ResponseBody
    public ResponseEntity<GETClienteDTO> encontrarPorCpf(@PathVariable String cpf) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorCpf(cpf), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETClienteDTO> salvar(@RequestBody ClienteDTO ClienteDTO) {
        try {
            return new ResponseEntity<>(this.service.salvar(ClienteDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public ResponseEntity<GETClienteDTO> editar(@RequestBody ClienteDTO ClienteDTO, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.editar(ClienteDTO, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
