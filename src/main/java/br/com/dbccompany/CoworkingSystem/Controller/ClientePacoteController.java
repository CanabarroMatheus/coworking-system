package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.ClientePacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETClientePacoteDTO;
import br.com.dbccompany.CoworkingSystem.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientePacote")
public class ClientePacoteController {

    @Autowired
    private ClientePacoteService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETClientePacoteDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETClientePacoteDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETClientePacoteDTO> salvar(@RequestBody ClientePacoteDTO clientePacoteDTO) {
        try {
            return new ResponseEntity<>(this.service.salvar(clientePacoteDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
