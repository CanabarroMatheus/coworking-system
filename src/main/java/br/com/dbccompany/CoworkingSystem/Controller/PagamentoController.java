package br.com.dbccompany.CoworkingSystem.Controller;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETPagamentoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.PagamentoDTO;
import br.com.dbccompany.CoworkingSystem.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService service;

    @GetMapping("/trazer/todos")
    @ResponseBody
    public ResponseEntity<List<GETPagamentoDTO>> trazerTodos() {
        try {
            return new ResponseEntity<>(this.service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/id/{id}")
    @ResponseBody
    public ResponseEntity<GETPagamentoDTO> encontrarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(this.service.encontrarPorId(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/salvar")
    @ResponseBody
    public ResponseEntity<GETPagamentoDTO> pagar(@RequestBody PagamentoDTO pagamentoDTO) {
        try {
            return new ResponseEntity<>(this.service.efetuarPagamento(pagamentoDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
