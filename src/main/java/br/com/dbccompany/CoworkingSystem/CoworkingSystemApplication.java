package br.com.dbccompany.CoworkingSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoworkingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoworkingSystemApplication.class, args);
	}

}
