package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import br.com.dbccompany.CoworkingSystem.Util.MoedaUtil;

import java.util.ArrayList;
import java.util.List;

public class PacoteDTO {
    private Integer id;
    private String valor;
    private List<EspacoPacoteDTO> espacoPacote;

    public PacoteDTO() {
    }

    public PacoteDTO(PacoteEntity pacoteEntity) {
        this.id = pacoteEntity.getId() != null
                ? pacoteEntity.getId()
                : null;
        this.valor = MoedaUtil.converter(pacoteEntity.getValor());
        this.espacoPacote = pacoteEntity.getEspacoPacote() != null
                ? this.converterListaEspacoPacoteDTO(pacoteEntity.getEspacoPacote())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteDTO> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<EspacoPacoteDTO> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public PacoteEntity converter() {
        PacoteEntity pacoteEntity = new PacoteEntity();

        pacoteEntity.setId(this.id != null
                ? this.id
                : null);
        pacoteEntity.setValor(MoedaUtil.limparCurrencyString(this.valor));
        pacoteEntity.setEspacoPacote(this.espacoPacote != null
                ? this.converterListaEspacoPacoteEntity(this.espacoPacote)
                : null);

        return pacoteEntity;
    }

    private List<EspacoPacoteDTO> converterListaEspacoPacoteDTO(List<EspacoPacoteEntity> espacoPacoteEntities) {
        ArrayList<EspacoPacoteDTO> espacoPacoteDTOS = new ArrayList<>();

        for (EspacoPacoteEntity espacoPacoteEntity : espacoPacoteEntities) {
            espacoPacoteDTOS.add(new EspacoPacoteDTO(espacoPacoteEntity));
        }

        return espacoPacoteDTOS;
    }

    private List<EspacoPacoteEntity> converterListaEspacoPacoteEntity(List<EspacoPacoteDTO> espacoPacoteDTOS) {
        ArrayList<EspacoPacoteEntity> espacoPacoteEntities = new ArrayList<>();

        for (EspacoPacoteDTO espacoPacoteDTO : espacoPacoteDTOS) {
            espacoPacoteEntities.add(espacoPacoteDTO.converter());
        }

        return espacoPacoteEntities;
    }
}
