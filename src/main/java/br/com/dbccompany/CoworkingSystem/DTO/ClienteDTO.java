package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.*;
import br.com.dbccompany.CoworkingSystem.Util.CPFUtil;

import java.time.LocalDate;
import java.util.ArrayList;

import java.util.List;

public class ClienteDTO {
    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private List<ContatoDTO> contatos;
    private List<ContratacaoDTO> contratacao;

    public ClienteDTO() {
    }

    public ClienteDTO(ClienteEntity clienteEntity) {
        this.id = clienteEntity.getId() != null ? clienteEntity.getId() : null;
        this.nome = clienteEntity.getNome();
        this.cpf = CPFUtil.converterCharParaString(clienteEntity.getCpf());
        this.dataNascimento = clienteEntity.getDataNascimento();
        this.contatos = clienteEntity.getContatos() != null
                ? this.converterListaContatosDTO(clienteEntity.getContatos())
                : null ;
        this.contratacao = clienteEntity.getContratacao() != null
                ? this.converterListaContratacaoDTO(clienteEntity.getContratacao())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoDTO> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoDTO> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoDTO> contratacao) {
        this.contratacao = contratacao;
    }

    public ClienteEntity converter() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(this.id != null ? this.id : null);
        cliente.setNome(this.nome);
        cliente.setCpf(CPFUtil.converterStringParaChar(this.cpf));
        cliente.setDataNascimento(this.dataNascimento);
        cliente.setContatos(this.contatos != null
                ? this.converterListaContatosEntity(this.contatos)
                : null);
        cliente.setContratacao(this.contratacao != null
                ? this.converterListaContratacaoEntity(this.contratacao)
                : null);

        return cliente;
    }

    private List<ContatoDTO> converterListaContatosDTO(List<ContatoEntity> contatoEntities) {
        ArrayList<ContatoDTO> contatos = new ArrayList<>();

        for (ContatoEntity contato : contatoEntities) {
            contato.setCliente(null);
            contatos.add(new ContatoDTO(contato));
        }

        return contatos;
    }

    private List<ContatoEntity> converterListaContatosEntity(List<ContatoDTO> contatosDTO) {
        ArrayList<ContatoEntity> contatos = new ArrayList<>();

        for (ContatoDTO contato : contatosDTO) {
            contatos.add(contato.converter());
        }

        return contatos;
    }

    private List<ContratacaoDTO> converterListaContratacaoDTO(List<ContratacaoEntity> contratacaoEntities) {
        ArrayList<ContratacaoDTO> contratacaoDTOS = new ArrayList<>();

        for (ContratacaoEntity contratacaoEntity : contratacaoEntities) {
            contratacaoDTOS.add(new ContratacaoDTO(contratacaoEntity));
        }

        return contratacaoDTOS;
    }

    private List<ContratacaoEntity> converterListaContratacaoEntity(List<ContratacaoDTO> contratacaoDTOS) {
        ArrayList<ContratacaoEntity> contratacaoEntities = new ArrayList<>();

        for (ContratacaoDTO contratacaoDTO : contratacaoDTOS) {
            contratacaoEntities.add(contratacaoDTO.converter());
        }

        return contratacaoEntities;
    }
}
