package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.UsuarioEntity;

public class UsuarioDTO {

    private Integer id;
    private String nome;
    private String email;
    private String login;
    private String senha;

    public UsuarioDTO() {
    }

    public UsuarioDTO(UsuarioEntity usuarioEntity) {
        this.id = usuarioEntity.getId() != null
                ? usuarioEntity.getId()
                : null;
        this.nome = usuarioEntity.getNome();
        this.email = usuarioEntity.getEmail();
        this.login = usuarioEntity.getLogin();
        this.senha = usuarioEntity.getSenha();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public UsuarioEntity converter() {
        UsuarioEntity usuarioEntity = new UsuarioEntity();

        usuarioEntity.setId(this.id != null
                ? this.id
                : null);
        usuarioEntity.setNome(this.nome);
        usuarioEntity.setEmail(this.email);
        usuarioEntity.setLogin(this.login);
        usuarioEntity.setSenha(this.senha);

        return usuarioEntity;
    }
}
