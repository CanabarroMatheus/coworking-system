package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;

import java.time.LocalDate;

public class GETEspacoPacoteDTO {
    private Integer id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;
    private GETEspacoDTO espaco;

    public GETEspacoPacoteDTO() {
    }

    public GETEspacoPacoteDTO(EspacoPacoteEntity espacoPacoteEntity) {
        this.id = espacoPacoteEntity.getId();
        this.tipoContratacao = espacoPacoteEntity.getTipoContratacao();
        this.quantidade = espacoPacoteEntity.getQuantidade();
        this.prazo = espacoPacoteEntity.getPrazo();
        this.espaco = new GETEspacoDTO(espacoPacoteEntity.getEspaco());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public GETEspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(GETEspacoDTO espaco) {
        this.espaco = espaco;
    }
}
