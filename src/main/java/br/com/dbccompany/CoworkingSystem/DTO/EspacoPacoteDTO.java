package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;

import java.time.LocalDate;
import java.util.List;

public class EspacoPacoteDTO {
    private Integer id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;
    private EspacoDTO espaco;

    public EspacoPacoteDTO() {
    }

    public EspacoPacoteDTO(EspacoPacoteEntity espacoPacoteEntity) {
        this.id = espacoPacoteEntity.getId() != null
                ? espacoPacoteEntity.getId() : null;
        this.tipoContratacao = espacoPacoteEntity.getTipoContratacao();
        this.quantidade = espacoPacoteEntity.getQuantidade();
        this.prazo = espacoPacoteEntity.getPrazo();
        this.espaco = espacoPacoteEntity.getEspaco() != null
                ? new EspacoDTO(espacoPacoteEntity.getEspaco())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public EspacoPacoteEntity converter() {
        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();

        espacoPacoteEntity.setId(this.id != null
                ? this.id
                : null);
        espacoPacoteEntity.setTipoContratacao(this.tipoContratacao);
        espacoPacoteEntity.setQuantidade(this.quantidade);
        espacoPacoteEntity.setPrazo(this.prazo);
        espacoPacoteEntity.setEspaco(this.espaco != null
                ? this.espaco.converter()
                : null);

        return espacoPacoteEntity;
    }
}
