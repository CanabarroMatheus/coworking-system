package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Util.MoedaUtil;

public class GETEspacoDTO {
    private Integer id;
    private String nome;
    private Integer quantidadePessoas;
    private String valor;

    public GETEspacoDTO() {
    }

    public GETEspacoDTO(EspacoEntity espacoEntity) {
        this.id = espacoEntity.getId();
        this.nome = espacoEntity.getNome();
        this.quantidadePessoas = espacoEntity.getQuantidadePessoas();
        this.valor = MoedaUtil.converter(espacoEntity.getValor());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Integer quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
