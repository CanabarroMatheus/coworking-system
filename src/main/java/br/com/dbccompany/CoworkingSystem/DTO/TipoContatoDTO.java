package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TipoContatoDTO {
    private Integer id;
    private String nome;

    public TipoContatoDTO() {
    }

    public TipoContatoDTO(TipoContatoEntity tipoContatoEntity) {
        this.id = tipoContatoEntity.getId() != null ? tipoContatoEntity.getId() : null;
        this.nome = tipoContatoEntity.getNome();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    private HashSet<ContatoDTO> converterListaParaDTO(List<ContatoEntity> contatos) {
        HashSet<ContatoDTO> contatosDTO = new HashSet<>();

        for (ContatoEntity contato : contatos) {
            contatosDTO.add(new ContatoDTO(contato));
        }

        return contatosDTO;
    }

    private List<ContatoEntity> converterListaParaEntity(HashSet<ContatoDTO> contatosDTO) {
        ArrayList<ContatoEntity> contatos = new ArrayList<>();

        for (ContatoDTO contato : contatosDTO) {
            contatos.add(contato.converter());
        }

        return contatos;
    }

    public TipoContatoEntity converter() {
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();

        tipoContatoEntity.setId(this.id != null ? this.id : null);
        tipoContatoEntity.setNome(this.nome);
//        tipoContatoEntity.setContatos(this.contatos != null
//                ? this.converterListaParaEntity(this.contatos)
//                : null);

        return tipoContatoEntity;
    }
}
