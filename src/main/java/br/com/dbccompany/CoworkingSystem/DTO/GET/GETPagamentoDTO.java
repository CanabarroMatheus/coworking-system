package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.DTO.SaldoClienteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoPagamentoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;

import java.util.List;

public class GETPagamentoDTO {
    private Integer id;
    private TipoPagamentoEnum tipoPagamento;
    private GETClientePacoteDTO clientePacote;
    private GETContratacaoDTO contratacao;
    private List<SaldoClienteDTO> saldosGerados;

    public GETPagamentoDTO() {
    }

    public GETPagamentoDTO(PagamentoEntity pagamentoEntity) {
        this.id = pagamentoEntity.getId();
        this.tipoPagamento = pagamentoEntity.getTipoPagamento();
        this.clientePacote = pagamentoEntity.getClientePacote() != null
                ? new GETClientePacoteDTO(pagamentoEntity.getClientePacote())
                : null;
        this.contratacao = pagamentoEntity.getContratacao() != null
                ? new GETContratacaoDTO(pagamentoEntity.getContratacao())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public GETClientePacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(GETClientePacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public GETContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(GETContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteDTO> getSaldosGerados() {
        return saldosGerados;
    }

    public void setSaldosGerados(List<SaldoClienteDTO> saldosGerados) {
        this.saldosGerados = saldosGerados;
    }
}
