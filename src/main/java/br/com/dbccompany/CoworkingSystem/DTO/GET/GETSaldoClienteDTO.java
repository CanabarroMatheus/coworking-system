package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.DTO.ClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.EspacoDTO;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.Id.SaldoClienteId;
import br.com.dbccompany.CoworkingSystem.Entity.SaldoClienteEntity;
import org.springframework.data.annotation.Transient;

import java.time.LocalDate;
import java.util.Date;

public class GETSaldoClienteDTO {
    private SaldoClienteId id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;
    private GETEspacoDTO espaco;
    private GETClienteDTO cliente;

    public GETSaldoClienteDTO() {
    }

    public GETSaldoClienteDTO(SaldoClienteEntity saldoClienteEntity, String fluxo) {
        if (fluxo.equalsIgnoreCase("cliente")) {
            this.id = saldoClienteEntity.getId();
            this.tipoContratacao = saldoClienteEntity.getTipoContratacao();
            this.quantidade = saldoClienteEntity.getQuantidade();
            this.vencimento = saldoClienteEntity.getVencimento();
            this.espaco = new GETEspacoDTO(saldoClienteEntity.getEspaco());
            this.cliente = null;
        } else {
            this.id = saldoClienteEntity.getId();
            this.tipoContratacao = saldoClienteEntity.getTipoContratacao();
            this.quantidade = saldoClienteEntity.getQuantidade();
            this.vencimento = saldoClienteEntity.getVencimento();
            this.espaco = null;
            this.cliente = new GETClienteDTO(saldoClienteEntity.getCliente(), "espaco");
        }
    }

    public GETSaldoClienteDTO(SaldoClienteEntity saldoClienteEntity) {
        this.id = saldoClienteEntity.getId();
        this.tipoContratacao = saldoClienteEntity.getTipoContratacao();
        this.quantidade = saldoClienteEntity.getQuantidade();
        this.vencimento = saldoClienteEntity.getVencimento();
        this.espaco = new GETEspacoDTO(saldoClienteEntity.getEspaco());
        this.cliente = new GETClienteDTO(saldoClienteEntity.getCliente(), "espaco");
    }

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public GETEspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(GETEspacoDTO espaco) {
        this.espaco = espaco;
    }

    public GETClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(GETClienteDTO cliente) {
        this.cliente = cliente;
    }
}
