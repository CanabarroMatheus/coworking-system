package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.AcessoEntity;

import java.time.LocalDateTime;

public class GETAcessoDTO {
    private Integer id;
    private GETSaldoClienteDTO saldoCliente;
    private Boolean entrada;
    private LocalDateTime data;
    private Boolean excessao;
    private String mensagem;

    public GETAcessoDTO() {

    }

    public GETAcessoDTO(AcessoEntity acessoEntity) {
        this.id = acessoEntity.getId();
        this.saldoCliente = new GETSaldoClienteDTO(acessoEntity.getSaldoCliente());
        this.entrada = acessoEntity.getEntrada();
        this.data = acessoEntity.getData();
        this.excessao = acessoEntity.getExcessao();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GETSaldoClienteDTO getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(GETSaldoClienteDTO saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcessao() {
        return excessao;
    }

    public void setExcessao(Boolean excessao) {
        this.excessao = excessao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
