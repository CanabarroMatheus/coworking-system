package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;

public class ContatoDTO {
    private Integer id;
    private String valor;
    private TipoContatoDTO tipoContato;
    private ClienteDTO cliente;

    public ContatoDTO() {
    }

    public ContatoDTO(ContatoEntity contato) {
        this.id = contato.getId() != null ? contato.getId() : null;
        this.valor = contato.getValor();
        this.tipoContato = contato.getTipoContato() != null ? new TipoContatoDTO(contato.getTipoContato()) : null;
        this.cliente = contato.getCliente() != null ? new ClienteDTO(contato.getCliente()) : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public ContatoEntity converter() {
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id != null ? this.id : null);
        contato.setValor(this.valor);
        contato.setTipoContato(this.tipoContato != null ? this.tipoContato.converter() : null);
        contato.setCliente(this.cliente != null
                            ? this.cliente.converter()
                            : null);

        return contato;
    }
}
