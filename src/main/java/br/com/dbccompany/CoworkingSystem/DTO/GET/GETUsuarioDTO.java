package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.UsuarioEntity;

public class GETUsuarioDTO {
    private String nome;
    private String email;

    public GETUsuarioDTO() {
    }

    public GETUsuarioDTO(UsuarioEntity usuarioEntity) {
        this.nome = usuarioEntity.getNome();
        this.email = usuarioEntity.getEmail();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
