package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.*;
import br.com.dbccompany.CoworkingSystem.Util.CPFUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GETClienteDTO {
    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private List<GETContatoDTO> contatos;
    private List<GETSaldoClienteDTO> saldoCliente;

    public GETClienteDTO() {
    }

    public GETClienteDTO(ClienteEntity clienteEntity, String fluxo) {
        this.id = clienteEntity.getId();
        this.nome = clienteEntity.getNome();
        this.cpf = CPFUtil.converterCharParaString(clienteEntity.getCpf());
        this.dataNascimento = clienteEntity.getDataNascimento();
        this.contatos = this.converterContatosDTO(clienteEntity.getContatos());
        if (fluxo.equalsIgnoreCase("cliente")) {
            this.saldoCliente = clienteEntity.getSaldoCliente() != null
                    ? this.converterSaldosDTOCliente(clienteEntity.getSaldoCliente())
                    : null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<GETContatoDTO> getContatos() {
        return contatos;
    }

    public void setContatos(List<GETContatoDTO> contatos) {
        this.contatos = contatos;
    }

    public List<GETSaldoClienteDTO> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<GETSaldoClienteDTO> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    private List<GETContatoDTO> converterContatosDTO(List<ContatoEntity> contatoEntities) {
        ArrayList<GETContatoDTO> contatoDTOS = new ArrayList<>();

        for (ContatoEntity contatoEntity : contatoEntities) {
            contatoDTOS.add(new GETContatoDTO(contatoEntity));
        }

        return contatoDTOS;
    }

    private List<GETSaldoClienteDTO> converterSaldosDTOCliente(List<SaldoClienteEntity> saldoClienteEntities) {
        ArrayList<GETSaldoClienteDTO> saldoClienteDTOS = new ArrayList<>();

        for (SaldoClienteEntity saldoClienteEntity : saldoClienteEntities) {
            GETSaldoClienteDTO saldoClienteDTO = new GETSaldoClienteDTO(saldoClienteEntity, "cliente");
            saldoClienteDTOS.add(saldoClienteDTO);
        }

        return saldoClienteDTOS;
    }
}
