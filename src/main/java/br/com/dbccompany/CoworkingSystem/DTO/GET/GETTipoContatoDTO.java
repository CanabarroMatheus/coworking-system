package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;

public class GETTipoContatoDTO {
    private Integer id;
    private String nome;

    public GETTipoContatoDTO() {
    }

    public GETTipoContatoDTO(TipoContatoEntity tipoContatoEntity) {
        this.id = tipoContatoEntity.getId();
        this.nome = tipoContatoEntity.getNome();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
