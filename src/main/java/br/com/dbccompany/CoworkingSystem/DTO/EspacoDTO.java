package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Util.MoedaUtil;

public class EspacoDTO {

    private Integer id;
    private String nome;
    private Integer quantidadePessoas;
    private String valor;

    public EspacoDTO() {
    }

    public EspacoDTO(EspacoEntity espacoEntity) {
        this.id = espacoEntity.getId() != null ? espacoEntity.getId() : null;
        this.nome = espacoEntity.getNome();
        this.quantidadePessoas = espacoEntity.getQuantidadePessoas();
        this.valor = MoedaUtil.converter(espacoEntity.getValor());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Integer quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public EspacoEntity converter() {
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setId(this.id != null ? this.id : null);
        espacoEntity.setNome(this.nome);
        espacoEntity.setQuantidadePessoas(this.quantidadePessoas);
        espacoEntity.setValor(this.valor != null
                ? MoedaUtil.limparCurrencyString(this.valor)
                : null);

        return espacoEntity;
    }
}
