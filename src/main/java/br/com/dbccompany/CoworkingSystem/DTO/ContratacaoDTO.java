package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.ContratacaoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ContratacaoDTO {

    private Integer id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private String valorContracao;

    public ContratacaoDTO() {
    }

    public ContratacaoDTO(ContratacaoEntity contratacaoEntity) {
        this.id = contratacaoEntity.getId() != null
                ? contratacaoEntity.getId()
                : null;
        this.tipoContratacao = contratacaoEntity.getTipoContratacao();
        this.quantidade = contratacaoEntity.getQuantidade();
        this.desconto = contratacaoEntity.getDesconto() != null
                ? contratacaoEntity.getDesconto()
                : null;
        this.prazo = contratacaoEntity.getPrazo();
        this.cliente = contratacaoEntity.getCliente() != null
                ? new ClienteDTO(contratacaoEntity.getCliente())
                : null;
        this.espaco = contratacaoEntity.getEspaco() != null
                ? new EspacoDTO(contratacaoEntity.getEspaco())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public String getValorContracao() {
        return valorContracao;
    }

    public void setValorContracao(String valorContracao) {
        this.valorContracao = valorContracao;
    }

    public ContratacaoEntity converter() {
        ContratacaoEntity contratacaoEntity = new ContratacaoEntity();

        contratacaoEntity.setId(this.id != null
                ? this.id
                : null);
        contratacaoEntity.setTipoContratacao(this.tipoContratacao);
        contratacaoEntity.setQuantidade(this.quantidade);
        contratacaoEntity.setDesconto(this.desconto != null
                ? this.desconto
                : null);
        contratacaoEntity.setPrazo(this.prazo);
        contratacaoEntity.setCliente(this.cliente != null
                ? this.cliente.converter()
                : null);
        contratacaoEntity.setEspaco(this.espaco != null
                ? this.espaco.converter()
                : null);

        return contratacaoEntity;
    }
}
