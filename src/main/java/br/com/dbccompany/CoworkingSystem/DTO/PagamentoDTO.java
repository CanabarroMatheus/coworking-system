package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoPagamentoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;

import java.util.List;

public class PagamentoDTO {

    private Integer id;
    private TipoPagamentoEnum tipoPagamento;
    private ClientePacoteDTO clientePacote;
    private ContratacaoDTO contratacao;
    private List<SaldoClienteDTO> saldosGerado;

    public PagamentoDTO() {
    }

    public PagamentoDTO(PagamentoEntity pagamentoEntity) {
        this.id = pagamentoEntity.getId() != null
                ? pagamentoEntity.getId()
                : null;
        this.tipoPagamento = pagamentoEntity.getTipoPagamento();
        this.clientePacote = pagamentoEntity.getClientePacote() != null
                ? new ClientePacoteDTO(pagamentoEntity.getClientePacote())
                : null;
        this.contratacao = pagamentoEntity.getContratacao() != null
                ? new ContratacaoDTO(pagamentoEntity.getContratacao())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientePacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteDTO> getSaldosGerado() {
        return saldosGerado;
    }

    public void setSaldosGerado(List<SaldoClienteDTO> saldosGerado) {
        this.saldosGerado = saldosGerado;
    }

    public PagamentoEntity converter() {
        PagamentoEntity pagamentoEntity = new PagamentoEntity();

        pagamentoEntity.setId(this.id != null
                ? this.id
                : null);
        pagamentoEntity.setTipoPagamento(this.tipoPagamento);
        pagamentoEntity.setClientePacote(this.clientePacote != null
                ? this.clientePacote.converter()
                : null);
        pagamentoEntity.setContratacao(this.contratacao != null
                ? this.contratacao.converter()
                : null);

        return pagamentoEntity;
    }
}
