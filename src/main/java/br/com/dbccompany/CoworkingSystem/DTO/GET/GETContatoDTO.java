package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.DTO.TipoContatoDTO;
import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;

public class GETContatoDTO {
    private Integer id;
    private String valor;
    private GETTipoContatoDTO tipoContato;

    public GETContatoDTO() {
    }

    public GETContatoDTO(ContatoEntity contatoEntity) {
        this.id = contatoEntity.getId();
        this.valor = contatoEntity.getValor();
        this.tipoContato = new GETTipoContatoDTO(contatoEntity.getTipoContato());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public GETTipoContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(GETTipoContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }

}
