package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.AcessoEntity;

import java.time.LocalDateTime;

public class AcessoDTO {

    private Integer id;
    private SaldoClienteDTO saldoCliente;
    private Boolean entrada;
    private LocalDateTime data;
    private Boolean excessao = false;
    private String mensagem;

    public AcessoDTO() {
    }

    public AcessoDTO(AcessoEntity acessoEntity) {
        this.id = acessoEntity.getId() != null
                ? acessoEntity.getId()
                : null;
        this.saldoCliente = acessoEntity.getSaldoCliente() != null
                ? new SaldoClienteDTO(acessoEntity.getSaldoCliente())
                : null;
        this.entrada = acessoEntity.getEntrada();
        this.data = acessoEntity.getData();
        this.excessao = acessoEntity.getExcessao();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteDTO getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteDTO saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcessao() {
        return excessao;
    }

    public void setExcessao(Boolean excessao) {
        this.excessao = excessao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public AcessoEntity converter() {
        AcessoEntity acessoEntity = new AcessoEntity();

        acessoEntity.setId(this.id != null ? this.id : null);
        acessoEntity.setSaldoCliente(this.getSaldoCliente() != null
                ? this.saldoCliente.converter()
                : null);
        acessoEntity.setEntrada(this.entrada);
        acessoEntity.setData(this.data);
        acessoEntity.setExcessao(this.excessao);

        return acessoEntity;
    }

}
