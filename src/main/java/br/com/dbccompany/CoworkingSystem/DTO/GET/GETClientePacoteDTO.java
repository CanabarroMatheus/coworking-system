package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;

public class GETClientePacoteDTO {
    private Integer id;
    private Integer quantidade;
    private GETPacoteDTO pacote;
    private GETClienteDTO cliente;

    public GETClientePacoteDTO() {
    }

    public GETClientePacoteDTO(ClientePacoteEntity clientePacoteEntity) {
        this.id = clientePacoteEntity.getId();
        this.quantidade = clientePacoteEntity.getQuantidade();
        this.pacote = new GETPacoteDTO(clientePacoteEntity.getPacote());
        this.cliente = new GETClienteDTO(clientePacoteEntity.getCliente(), "pacote");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public GETPacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(GETPacoteDTO pacote) {
        this.pacote = pacote;
    }

    public GETClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(GETClienteDTO cliente) {
        this.cliente = cliente;
    }

}
