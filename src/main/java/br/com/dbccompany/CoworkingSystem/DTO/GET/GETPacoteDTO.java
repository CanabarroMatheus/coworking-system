package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import br.com.dbccompany.CoworkingSystem.Util.MoedaUtil;

import java.util.ArrayList;
import java.util.List;

public class GETPacoteDTO {
    private Integer id;
    private String valor;
    private List<GETEspacoPacoteDTO> espacoPacote;

    public GETPacoteDTO(PacoteEntity pacoteEntity) {
        this.id = pacoteEntity.getId();
        this.valor = MoedaUtil.converter(pacoteEntity.getValor());
        this.espacoPacote = pacoteEntity.getEspacoPacote() != null
                ? this.converterEspacosPacoteDTOpacote(pacoteEntity.getEspacoPacote())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<GETEspacoPacoteDTO> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<GETEspacoPacoteDTO> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<GETEspacoPacoteDTO> converterEspacosPacoteDTOpacote(List<EspacoPacoteEntity> espacoPacoteEntities) {
        ArrayList<GETEspacoPacoteDTO> espacoPacoteDTOS = new ArrayList<>();

        for (EspacoPacoteEntity espacoPacoteEntity : espacoPacoteEntities) {
            espacoPacoteDTOS.add(new GETEspacoPacoteDTO(espacoPacoteEntity));
        }

        return espacoPacoteDTOS;
    }
}
