package br.com.dbccompany.CoworkingSystem.DTO;

import java.time.LocalDateTime;

public class LogDTO {
    private String data;
    private String tipo;
    private String codigo;
    private String descricao;

    public LogDTO(String tipo, String codigo, Exception exception, String logMessagem) {
        this.data = LocalDateTime.now().toString();
        this.tipo = tipo;
        this.codigo = codigo;
        this.descricao = exception.getMessage() + " -- " + logMessagem;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
