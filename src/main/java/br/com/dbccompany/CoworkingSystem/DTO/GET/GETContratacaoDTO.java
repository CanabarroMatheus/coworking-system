package br.com.dbccompany.CoworkingSystem.DTO.GET;

import br.com.dbccompany.CoworkingSystem.Entity.ContratacaoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GETContratacaoDTO {
    private Integer id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private GETClienteDTO cliente;
    private GETEspacoDTO espaco;
    private String valorContracao;

    public GETContratacaoDTO() {
    }

    public GETContratacaoDTO(ContratacaoEntity contratacaoEntity) {
        this.id = contratacaoEntity.getId();
        this.tipoContratacao = contratacaoEntity.getTipoContratacao();
        this.quantidade = contratacaoEntity.getQuantidade();
        this.prazo = contratacaoEntity.getPrazo();
        this.espaco = contratacaoEntity.getEspaco() != null
                ? new GETEspacoDTO(contratacaoEntity.getEspaco())
                : null;
        this.cliente = contratacaoEntity.getCliente() != null
                ? new GETClienteDTO(contratacaoEntity.getCliente(), "espaco")
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public GETClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(GETClienteDTO cliente) {
        this.cliente = cliente;
    }

    public GETEspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(GETEspacoDTO espaco) {
        this.espaco = espaco;
    }

    public String getValorContracao() {
        return valorContracao;
    }

    public void setValorContracao(String valorContracao) {
        this.valorContracao = valorContracao;
    }

    private List<GETPagamentoDTO> converterListaPagamento(List<PagamentoEntity> pagamentoEntities) {
        ArrayList<GETPagamentoDTO> pagamentoDTOS = new ArrayList<>();

        for (PagamentoEntity pagamentoEntity : pagamentoEntities) {
            GETPagamentoDTO pagamentoDTO = new GETPagamentoDTO(pagamentoEntity);
            pagamentoDTOS.add(pagamentoDTO);
        }

        return pagamentoDTOS;
    }

}
