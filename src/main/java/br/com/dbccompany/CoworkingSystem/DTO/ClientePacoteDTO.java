package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;

import java.util.ArrayList;
import java.util.List;

public class ClientePacoteDTO {
    private Integer id;
    private ClienteDTO cliente;
    private PacoteDTO pacote;
    private Integer quantidade;

    public ClientePacoteDTO() {
    }

    public ClientePacoteDTO(ClientePacoteEntity clientePacoteEntity) {
        this.id = clientePacoteEntity.getId() != null
                ? clientePacoteEntity.getId() : null;
        this.quantidade = clientePacoteEntity.getQuantidade();
        this.pacote = clientePacoteEntity.getPacote() != null
                ? new PacoteDTO(clientePacoteEntity.getPacote())
                : null;
        this.cliente = clientePacoteEntity.getCliente() != null
                ? new ClienteDTO(clientePacoteEntity.getCliente())
                : null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public ClientePacoteEntity converter() {
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();
        clientePacoteEntity.setId(this.id != null
                ? this.id
                : null);
        clientePacoteEntity.setQuantidade(this.quantidade);
        clientePacoteEntity.setPacote(this.pacote != null
                ? this.pacote.converter()
                : null);
        clientePacoteEntity.setCliente(this.cliente != null
                ? this.cliente.converter()
                : null);

        return clientePacoteEntity;
    }
}
