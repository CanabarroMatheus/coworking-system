package br.com.dbccompany.CoworkingSystem.DTO;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.Id.SaldoClienteId;
import br.com.dbccompany.CoworkingSystem.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class SaldoClienteDTO {

    private SaldoClienteId id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;
    private EspacoDTO espaco;
    private ClienteDTO cliente;

    public SaldoClienteDTO() {
    }

    public SaldoClienteDTO(SaldoClienteEntity saldoClienteEntity) {
        this.id = saldoClienteEntity.getId() != null ? saldoClienteEntity.getId() : null;
        this.tipoContratacao = saldoClienteEntity.getTipoContratacao();
        this.quantidade = saldoClienteEntity.getQuantidade();
        this.vencimento = saldoClienteEntity.getVencimento();
        this.espaco = saldoClienteEntity.getEspaco() != null
                ? new EspacoDTO(saldoClienteEntity.getEspaco())
                : null;
        this.cliente = saldoClienteEntity.getCliente() != null
                ? new ClienteDTO(saldoClienteEntity.getCliente())
                : null;
    }

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public SaldoClienteEntity converter() {
        SaldoClienteEntity saldoClienteEntity = new SaldoClienteEntity();

        saldoClienteEntity.setId(this.id != null ? this.id : null);
        saldoClienteEntity.setTipoContratacao(this.tipoContratacao);
        saldoClienteEntity.setQuantidade(this.quantidade);
        saldoClienteEntity.setVencimento(this.vencimento);
        saldoClienteEntity.setEspaco(this.espaco != null
                ? this.espaco.converter()
                : null);
        saldoClienteEntity.setCliente(this.cliente != null
                ? this.cliente.converter()
                : null);

        return saldoClienteEntity;
    }
}
