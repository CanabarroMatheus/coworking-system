package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.AcessoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETAcessoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.SaldoClienteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.AcessoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.SaldoClienteEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.AcessoRepository;
import br.com.dbccompany.CoworkingSystem.Repository.ClienteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.SaldoClienteRepository;
import br.com.dbccompany.CoworkingSystem.Util.ConversorTempoUtil;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;
    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    public List<GETAcessoDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaGETDTO((List<AcessoEntity>) this.acessoRepository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETAcessoDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            return new GETAcessoDTO(this.acessoRepository.findById(id).orElse(null));
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETAcessoDTO entrar(AcessoDTO acessoDTO) throws ErroPersistenciaException {
        try {
            acessoDTO.setSaldoCliente(new SaldoClienteDTO(this.saldoClienteRepository.findById(acessoDTO.getSaldoCliente().getId()).get()));

            if(acessoDTO.getSaldoCliente().getQuantidade() <= 0 || acessoDTO.getSaldoCliente().getVencimento().isBefore(LocalDate.now())) {
                acessoDTO.getSaldoCliente().setQuantidade(0);
                this.saldoClienteRepository.save(acessoDTO.getSaldoCliente().converter());
                GETAcessoDTO acesso = new GETAcessoDTO();
                acesso.setMensagem("Saldo insuficiente ou vencido!");
                return acesso;
            }

            acessoDTO.setData(acessoDTO.getData() != null
                    ? acessoDTO.getData()
                    : LocalDateTime.now());

            acessoDTO.setEntrada(true);

            GETAcessoDTO acesso = new GETAcessoDTO(this.salvarEEditar(acessoDTO.converter()));
            acesso.setMensagem(statusSaldo(acesso.getSaldoCliente().getTipoContratacao(), acesso.getSaldoCliente().getQuantidade()));

            return acesso;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETAcessoDTO sair(AcessoDTO acessoDTO) throws ErroPersistenciaException {
        try {
            acessoDTO.setSaldoCliente(new SaldoClienteDTO(this.saldoClienteRepository.findById(acessoDTO.getSaldoCliente().getId()).get()));
            acessoDTO.setData(acessoDTO.getData() != null
                    ? acessoDTO.getData()
                    : LocalDateTime.now());
            acessoDTO.setEntrada(false);

            GETAcessoDTO ultimoAcesso = this.converterListaGETDTO(this.acessoRepository.findAllBySaldoClienteAndIsEntradaOrderByDataDesc(acessoDTO.getSaldoCliente().converter(), true)).get(0);
            Duration diferenca = Duration.between(ultimoAcesso.getData(), acessoDTO.getData());
            acessoDTO.getSaldoCliente().setQuantidade(ConversorTempoUtil.diferencaEntreTempos(acessoDTO.getSaldoCliente().getQuantidade(), diferenca, acessoDTO.getSaldoCliente().getTipoContratacao()));
            this.saldoClienteRepository.save(acessoDTO.getSaldoCliente().converter());

            GETAcessoDTO acesso = new GETAcessoDTO(acessoDTO.converter());
            acesso.setMensagem(statusSaldo(acesso.getSaldoCliente().getTipoContratacao(), acesso.getSaldoCliente().getQuantidade()));

            return acesso;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETAcessoDTO entradaEspecial(AcessoDTO acessoDTO) throws ErroPersistenciaException {
        try {
            acessoDTO.setSaldoCliente(new SaldoClienteDTO(this.saldoClienteRepository.findById(acessoDTO.getSaldoCliente().getId()).get()));

            acessoDTO.getSaldoCliente().setQuantidade(999);
            acessoDTO.getSaldoCliente().setVencimento(LocalDate.now().plusDays(1));
            this.saldoClienteRepository.save(acessoDTO.getSaldoCliente().converter());

            acessoDTO.setExcessao(true);
            acessoDTO.setEntrada(true);
            acessoDTO.setData(LocalDateTime.now());

            GETAcessoDTO acesso = new GETAcessoDTO(this.salvarEEditar(acessoDTO.converter()));
            acesso.setMensagem(statusSaldo(TipoContratacaoEnum.ESPECIAL, 999));

            return acesso;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private String statusSaldo(TipoContratacaoEnum tipoContratacao, Integer saldo) {
        StringBuilder status = new StringBuilder();

        status.append("Saldo: " + saldo + ", ");
        status.append("Tipo da contratação: " + tipoContratacao);

        return status.toString();
    }

    private AcessoEntity salvarEEditar(AcessoEntity acessoEntity) {
        return this.acessoRepository.save(acessoEntity);
    }


    private List<GETAcessoDTO> converterListaGETDTO(List<AcessoEntity> acessoEntities) {
        ArrayList<GETAcessoDTO> acessoDTOS = new ArrayList<>();

        for(AcessoEntity acessoEntity : acessoEntities) {
            acessoDTOS.add(new GETAcessoDTO(acessoEntity));
        }

        return acessoDTOS;
    }

}
