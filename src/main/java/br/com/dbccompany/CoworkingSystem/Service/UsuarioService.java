package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETUsuarioDTO;
import br.com.dbccompany.CoworkingSystem.DTO.UsuarioDTO;
import br.com.dbccompany.CoworkingSystem.Entity.UsuarioEntity;
import br.com.dbccompany.CoworkingSystem.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public GETUsuarioDTO salvarUsuario(UsuarioDTO usuarioDTO) {
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        usuarioDTO.setSenha(bcryptEncoder.encode(usuarioDTO.getSenha()));
        UsuarioEntity usuarioEntity = this.salvarEEditar(usuarioDTO.converter());
        return new GETUsuarioDTO(usuarioEntity);
    }

    private UsuarioEntity salvarEEditar(UsuarioEntity usuarioEntity) {
        return this.repository.save(usuarioEntity);
    }

}
