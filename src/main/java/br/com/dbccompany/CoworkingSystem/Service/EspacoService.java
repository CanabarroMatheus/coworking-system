package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.EspacoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETEspacoDTO;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.EspacoRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    public List<GETEspacoDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<EspacoEntity>) this.repository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETEspacoDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            Optional<EspacoEntity> espacoEntity = this.repository.findById(id);
            if (espacoEntity.isPresent()) {
                return new GETEspacoDTO(espacoEntity.get());
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETEspacoDTO salvar(EspacoDTO espacoDTO) throws ErroPersistenciaException {
        try {
            EspacoEntity espacoEntity = this.salvarEEditar(espacoDTO.converter());
            return new GETEspacoDTO(espacoEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETEspacoDTO editar(EspacoDTO espacoDTO, Integer id) throws ErroPersistenciaException {
        try {
            espacoDTO.setId(id);
            EspacoEntity espacoEntity = this.salvarEEditar(espacoDTO.converter());
            return new GETEspacoDTO(espacoEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private EspacoEntity salvarEEditar(EspacoEntity espacoEntity) {
        return this.repository.save(espacoEntity);
    }

    private List<GETEspacoDTO> converterListaDTO(List<EspacoEntity> espacoEntities) {
        ArrayList<GETEspacoDTO> espacos = new ArrayList<>();

        for (EspacoEntity espacoEntity : espacoEntities) {
            espacos.add(new GETEspacoDTO(espacoEntity));
        }

        return espacos;
    }

}
