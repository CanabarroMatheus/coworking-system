package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.ClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.ContratacaoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.EspacoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETContratacaoDTO;
import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ContratacaoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.ClienteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.ContratacaoRepository;
import br.com.dbccompany.CoworkingSystem.Repository.EspacoRepository;
import br.com.dbccompany.CoworkingSystem.Util.ConversorTempoUtil;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import br.com.dbccompany.CoworkingSystem.Util.MoedaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private EspacoRepository espacoRepository;

    public List<GETContratacaoDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<ContratacaoEntity>) this.contratacaoRepository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETContratacaoDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            ContratacaoEntity contratacaoEntity = this.contratacaoRepository.findById(id).orElse(null);
            GETContratacaoDTO getContratacaoDTO = new GETContratacaoDTO(contratacaoEntity);
            getContratacaoDTO.setValorContracao(this.calcularValorCobrado(contratacaoEntity));
            return getContratacaoDTO;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETContratacaoDTO salvar(ContratacaoDTO contratacaoDTO) throws ErroPersistenciaException {
        try {
            ClienteEntity cliente = this.clienteRepository.findById(contratacaoDTO.getCliente().getId()).orElse(null);
            EspacoEntity espaco = this.espacoRepository.findById(contratacaoDTO.getEspaco().getId()).orElse(null);

            contratacaoDTO.setCliente(new ClienteDTO(cliente));
            contratacaoDTO.setEspaco(new EspacoDTO(espaco));

            ContratacaoEntity contratacaoEntity = this.salvarEEditar(contratacaoDTO.converter());

            GETContratacaoDTO getContratacaoDTO = new GETContratacaoDTO(contratacaoEntity);
            getContratacaoDTO.setValorContracao(this.calcularValorCobrado(contratacaoEntity));

            return getContratacaoDTO;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETContratacaoDTO editar(ContratacaoDTO contratacaoDTO, Integer id) throws ErroPersistenciaException {
        try {
            contratacaoDTO.setId(id);

            ContratacaoEntity contratacaoEntity = this.salvarEEditar(contratacaoDTO.converter());
            return new GETContratacaoDTO(contratacaoEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private String calcularValorCobrado(ContratacaoEntity contratacaoEntity) {
        if (contratacaoEntity.getDesconto() != null) {
            Double valor = contratacaoEntity.getEspaco().getValor() * ConversorTempoUtil.converterParaMinutos(contratacaoEntity.getQuantidade(), contratacaoEntity.getTipoContratacao());
            Double valorDesconto = valor * contratacaoEntity.getDesconto();
            return MoedaUtil.converter(valor - valorDesconto);
        } else {
            Double valor = contratacaoEntity.getEspaco().getValor() * ConversorTempoUtil.converterParaMinutos(contratacaoEntity.getQuantidade(), contratacaoEntity.getTipoContratacao());
            return MoedaUtil.converter(valor);
        }
    }

    private ContratacaoEntity salvarEEditar(ContratacaoEntity contratacaoEntity) {
        return this.contratacaoRepository.save(contratacaoEntity);
    }

    private List<GETContratacaoDTO> converterListaDTO(List<ContratacaoEntity> contratacaoEntities) {
        ArrayList<GETContratacaoDTO> contratacaoDTOS = new ArrayList<>();

        for (ContratacaoEntity contratacaoEntity : contratacaoEntities) {
            GETContratacaoDTO getContratacaoDTO = new GETContratacaoDTO(contratacaoEntity);
            getContratacaoDTO.setValorContracao(this.calcularValorCobrado(contratacaoEntity));
            contratacaoDTOS.add(getContratacaoDTO);
        }

        return contratacaoDTOS;
    }

}
