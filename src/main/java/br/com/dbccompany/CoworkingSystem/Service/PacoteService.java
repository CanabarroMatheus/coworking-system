package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETPacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.PacoteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.PacoteRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository repository;

    public List<GETPacoteDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<PacoteEntity>) this.repository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETPacoteDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            return new GETPacoteDTO(this.repository.findById(id).orElse(null));
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETPacoteDTO salvar(PacoteDTO pacoteDTO) throws ErroPersistenciaException {
        try {
            PacoteEntity pacote = this.salvarEEditar(pacoteDTO.converter());
            return new GETPacoteDTO(pacote);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETPacoteDTO editar(PacoteDTO pacoteDTO, Integer id) throws ErroPersistenciaException {
        try {
            pacoteDTO.setId(id);
            PacoteEntity pacote = this.salvarEEditar(pacoteDTO.converter());
            return new GETPacoteDTO(pacote);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private PacoteEntity salvarEEditar(PacoteEntity pacoteEntity) {
        return this.repository.save(pacoteEntity);
    }

    private List<GETPacoteDTO> converterListaDTO(List<PacoteEntity> pacoteEntities) {
        ArrayList<GETPacoteDTO> pacoteDTOS = new ArrayList<>();

        for (PacoteEntity pacoteEntity : pacoteEntities) {
            pacoteDTOS.add(new GETPacoteDTO(pacoteEntity));
        }

        return pacoteDTOS;
    }

}
