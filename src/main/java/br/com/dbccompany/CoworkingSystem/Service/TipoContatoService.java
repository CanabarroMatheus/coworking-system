package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETTipoContatoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.TipoContatoDTO;
import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroNaOperacaoException;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.TipoContatoRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    public List<GETTipoContatoDTO> trazerTodosTiposDeContatos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaParaDTO((List<TipoContatoEntity>) this.repository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public List<GETTipoContatoDTO> trazerTodosTiposDeContatosPorNome(List<String> nomes) throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaParaDTO(this.repository.findAllByNomeIn(nomes));
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETTipoContatoDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            Optional<TipoContatoEntity> tipoContato = this.repository.findById(id);
            if (tipoContato.isPresent()) {
                return new GETTipoContatoDTO(tipoContato.get());
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETTipoContatoDTO encontrarPorNome(String nome) throws RetornoBuscaInvalidoException {
        try {
            Optional<TipoContatoEntity> tipoContato = this.repository.findByNome(nome);
            if (tipoContato.isPresent()) {
                return new GETTipoContatoDTO(tipoContato.get());
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETTipoContatoDTO salvar(TipoContatoDTO tipoContato) throws ErroPersistenciaException {
        try {
            TipoContatoEntity tipoContatoEntity = this.salvarEEditar(tipoContato.converter());
            return new GETTipoContatoDTO(tipoContatoEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETTipoContatoDTO editar(TipoContatoDTO tipoContato, Integer id) throws ErroPersistenciaException {
        try {
            tipoContato.setId(id);
            TipoContatoEntity tipoContatoEntity = this.salvarEEditar(tipoContato.converter());
            return new GETTipoContatoDTO(tipoContatoEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private TipoContatoEntity salvarEEditar(TipoContatoEntity tipoContato) {
        return this.repository.save(tipoContato);
    }

    private List<GETTipoContatoDTO> converterListaParaDTO(List<TipoContatoEntity> tipoContatoEntities) {
        ArrayList<GETTipoContatoDTO> tipoContatoDTOS = new ArrayList<>();

        for (TipoContatoEntity tipoContato : tipoContatoEntities) {
            tipoContatoDTOS.add(new GETTipoContatoDTO(tipoContato));
        }

        return tipoContatoDTOS;
    }

}
