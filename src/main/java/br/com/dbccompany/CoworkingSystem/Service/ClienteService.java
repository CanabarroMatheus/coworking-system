package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.ClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.ContatoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETClienteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.ClienteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.ContatoRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ContatoService contatoService;

    public List<GETClienteDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<ClienteEntity>) this.clienteRepository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public List<GETClienteDTO> trazerTodosPorNomes(List<String> nomes) throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO(this.clienteRepository.findAllByNomeIn(nomes));
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public List<GETClienteDTO> trazerTodosPorDataNascimento(Date dataNascimento) throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO(this.clienteRepository.findAllByDataNascimento(dataNascimento));
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public List<GETClienteDTO> trazerTodosPorDataNascimentoIn(List<Date> datasNascimento) throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO(this.clienteRepository.findAllByDataNascimentoIn(datasNascimento));
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETClienteDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException{
        try {
            Optional<ClienteEntity> cliente = this.clienteRepository.findById(id);
            if (cliente.isPresent()) {
                return new GETClienteDTO(cliente.get(), "cliente");
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETClienteDTO encontrarPorCpf(String CPF) throws RetornoBuscaInvalidoException {
        try {
            Optional<ClienteEntity> cliente = this.clienteRepository.findByCpf(CPF);
            if (cliente.isPresent()) {
                return new GETClienteDTO(cliente.get(), "cliente");
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETClienteDTO salvar(ClienteDTO clienteDTO) throws ErroPersistenciaException {
        try {
            if (verificaTelefoneEEmail(clienteDTO.getContatos())) {
                ClienteEntity cliente = this.salvarEEditar(clienteDTO.converter());
                return new GETClienteDTO(cliente, "cliente");
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETClienteDTO editar(ClienteDTO clienteDTO, Integer id) throws ErroPersistenciaException {
        try {
            if (verificaTelefoneEEmail(clienteDTO.getContatos())) {
                clienteDTO.setId(id);
                ClienteEntity cliente = this.salvarEEditar(clienteDTO.converter());
                return new GETClienteDTO(cliente, "cliente");
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private Boolean verificaTelefoneEEmail(List<ContatoDTO> contatoDTOS) {
        boolean temEmail = false;
        boolean temTelefone = false;

        for (ContatoDTO contatoDTO : contatoDTOS) {
            if (contatoDTO.getTipoContato().getNome().equals("email")) {
                temEmail = true;
            }
            if (contatoDTO.getTipoContato().getNome().equals("telefone")) {
                temTelefone = true;
            }
        }

        return temEmail && temTelefone;
    }

    private ClienteEntity salvarEEditar(ClienteEntity clienteEntity) {
        return this.clienteRepository.save(clienteEntity);
    }

    private List<GETClienteDTO> converterListaDTO(List<ClienteEntity> clienteEntities) {
        ArrayList<GETClienteDTO> clientes = new ArrayList<>();

        for (ClienteEntity cliente : clienteEntities) {
            clientes.add(new GETClienteDTO(cliente, "cliente"));
        }

        return clientes;
    }
}
