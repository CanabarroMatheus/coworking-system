package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.ClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.ClientePacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETClientePacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.PacoteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.ClientePacoteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.ClienteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.PacoteRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientePacoteService {

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private PacoteRepository pacoteRepository;

    public List<GETClientePacoteDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<ClientePacoteEntity>) this.clientePacoteRepository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETClientePacoteDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            return new GETClientePacoteDTO(this.clientePacoteRepository.findById(id).orElse(null));
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETClientePacoteDTO salvar(ClientePacoteDTO clientePacoteDTO) throws ErroPersistenciaException {
        try {
            ClienteDTO cliente = new ClienteDTO(this.clienteRepository.findById(clientePacoteDTO.getCliente().getId()).orElse(null));
            PacoteDTO pacote = new PacoteDTO(this.pacoteRepository.findById(clientePacoteDTO.getPacote().getId()).orElse(null));

            clientePacoteDTO.setCliente(cliente);
            clientePacoteDTO.setPacote(pacote);

            ClientePacoteEntity clientePacoteEntity = this.salvarEEditar(clientePacoteDTO.converter());
            return new GETClientePacoteDTO(clientePacoteEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private ClientePacoteEntity salvarEEditar(ClientePacoteEntity clientePacoteEntity) {
        return this.clientePacoteRepository.save(clientePacoteEntity);
    }

    private List<GETClientePacoteDTO> converterListaDTO(List<ClientePacoteEntity> clientePacoteEntities) {
        ArrayList<GETClientePacoteDTO> clientePacoteDTOS = new ArrayList<>();

        for (ClientePacoteEntity clientePacoteEntity : clientePacoteEntities) {
            clientePacoteDTOS.add(new GETClientePacoteDTO(clientePacoteEntity));
        }

        return clientePacoteDTOS;
    }

}
