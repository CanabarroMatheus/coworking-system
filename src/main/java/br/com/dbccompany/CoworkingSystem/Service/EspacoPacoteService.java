package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.EspacoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.EspacoPacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETEspacoPacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.PacoteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.EspacoPacoteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.EspacoRepository;
import br.com.dbccompany.CoworkingSystem.Repository.PacoteRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacoPacoteService {

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;
    @Autowired
    private PacoteRepository pacoteRepository;
    @Autowired
    private EspacoRepository espacoRepository;

    public List<GETEspacoPacoteDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<EspacoPacoteEntity>) this.espacoPacoteRepository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETEspacoPacoteDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            return new GETEspacoPacoteDTO(this.espacoPacoteRepository.findById(id).orElse(null));
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETEspacoPacoteDTO salvar(EspacoPacoteDTO espacoPacoteDTO) throws ErroPersistenciaException {
        try {
            EspacoPacoteEntity espacoPacoteEntity = this.salvarEEditar(espacoPacoteDTO.converter());
            return new GETEspacoPacoteDTO(espacoPacoteEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETEspacoPacoteDTO editar(EspacoPacoteDTO espacoPacoteDTO, Integer id) throws ErroPersistenciaException {
        try {
            espacoPacoteDTO.setId(id);
            EspacoPacoteEntity espacoPacoteEntity = this.salvarEEditar(espacoPacoteDTO.converter());
            return new GETEspacoPacoteDTO(espacoPacoteEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private EspacoPacoteEntity salvarEEditar(EspacoPacoteEntity espacoPacoteEntity) {
        return this.espacoPacoteRepository.save(espacoPacoteEntity);
    }

    private List<GETEspacoPacoteDTO> converterListaDTO(List<EspacoPacoteEntity> espacoPacoteEntities) {
        ArrayList<GETEspacoPacoteDTO> espacoPacoteDTOS = new ArrayList<>();

        for (EspacoPacoteEntity espacoPacoteEntity : espacoPacoteEntities) {
            espacoPacoteDTOS.add(new GETEspacoPacoteDTO(espacoPacoteEntity));
        }

        return espacoPacoteDTOS;
    }
}
