package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.GET.GETSaldoClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.SaldoClienteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.*;
import br.com.dbccompany.CoworkingSystem.Entity.Id.SaldoClienteId;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.ClienteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.EspacoRepository;
import br.com.dbccompany.CoworkingSystem.Repository.SaldoClienteRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private EspacoRepository espacoRepository;

    public List<GETSaldoClienteDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<SaldoClienteEntity>) this.repository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETSaldoClienteDTO salvar(SaldoClienteDTO saldoClienteDTO) throws ErroPersistenciaException {
        try {
            SaldoClienteEntity saldoClienteEntity = this.salvarEEditar(saldoClienteDTO.converter());
            return new GETSaldoClienteDTO(saldoClienteEntity);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity gerarSaldoContratacao(ContratacaoEntity contratacaoEntity) throws ErroPersistenciaException {
        try {
            SaldoClienteId id = new SaldoClienteId();
            id.setId_cliente(contratacaoEntity.getCliente().getId());
            id.setId_espaco(contratacaoEntity.getEspaco().getId());

            if (verificarSaldoExiste(id)) {
                SaldoClienteEntity saldoCliente = this.repository.findById(id).get();

                saldoCliente.setQuantidade(saldoCliente.getQuantidade() + contratacaoEntity.getQuantidade());
                saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays(contratacaoEntity.getPrazo()));

                return this.salvarEEditar(saldoCliente);
            } else {
                SaldoClienteEntity saldoCliente = new SaldoClienteEntity();

                saldoCliente.setId(id);
                saldoCliente.setCliente(contratacaoEntity.getCliente());
                saldoCliente.setEspaco(contratacaoEntity.getEspaco());
                saldoCliente.setTipoContratacao(contratacaoEntity.getTipoContratacao());
                saldoCliente.setQuantidade(contratacaoEntity.getQuantidade());
                saldoCliente.setVencimento(LocalDate.now().plusDays((Integer) contratacaoEntity.getPrazo()));

                return this.salvarEEditar(saldoCliente);
            }
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public List<SaldoClienteEntity> gerarSaldoClientePacote(ClientePacoteEntity clientePacoteEntity) throws ErroPersistenciaException {
        try {
            ArrayList<SaldoClienteEntity> saldoClienteEntities = new ArrayList<>();

            for (EspacoPacoteEntity espacoPacoteEntity : clientePacoteEntity.getPacote().getEspacoPacote()) {
                SaldoClienteId id = new SaldoClienteId();

                id.setId_cliente(clientePacoteEntity.getCliente().getId());
                id.setId_espaco(espacoPacoteEntity.getEspaco().getId());

                if (verificarSaldoExiste(id)){
                    SaldoClienteEntity saldoClienteEntity = this.repository.findById(id).get();

                    saldoClienteEntity.setQuantidade(saldoClienteEntity.getQuantidade() + (clientePacoteEntity.getQuantidade() * espacoPacoteEntity.getQuantidade()));
                    saldoClienteEntity.setVencimento(saldoClienteEntity.getVencimento().plusDays(clientePacoteEntity.getQuantidade() * espacoPacoteEntity.getPrazo()));

                    saldoClienteEntities.add(saldoClienteEntity);
                } else {
                    SaldoClienteEntity saldoClienteEntity = new SaldoClienteEntity();

                    saldoClienteEntity.setId(id);
                    saldoClienteEntity.setCliente(clientePacoteEntity.getCliente());
                    saldoClienteEntity.setEspaco(espacoPacoteEntity.getEspaco());
                    saldoClienteEntity.setTipoContratacao(espacoPacoteEntity.getTipoContratacao());
                    saldoClienteEntity.setQuantidade(clientePacoteEntity.getQuantidade() * espacoPacoteEntity.getQuantidade());
                    saldoClienteEntity.setVencimento(LocalDate.now().plusDays(clientePacoteEntity.getQuantidade() * espacoPacoteEntity.getPrazo()));

                    saldoClienteEntities.add(saldoClienteEntity);
                }
            }

            return saldoClienteEntities;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private Boolean verificarSaldoExiste(SaldoClienteId id) {
        return this.repository.findById(id).isPresent();
    }

    private SaldoClienteEntity salvarEEditar(SaldoClienteEntity saldoClienteEntity) {
        EspacoEntity espaco = this.espacoRepository.findById(saldoClienteEntity.getId().getId_espaco())
                .orElse(null);
        ClienteEntity clienteEntity = this.clienteRepository.findById(saldoClienteEntity.getId().getId_cliente())
                .orElse(null);

        saldoClienteEntity.setEspaco(espaco);
        saldoClienteEntity.setCliente(clienteEntity);

        return this.repository.save(saldoClienteEntity);
    }

    private List<GETSaldoClienteDTO> converterListaDTO(List<SaldoClienteEntity> saldoClienteEntities) {
        ArrayList<GETSaldoClienteDTO> saldoClienteDTOS = new ArrayList<>();

        for (SaldoClienteEntity saldoClienteEntity : saldoClienteEntities) {
            saldoClienteDTOS.add(new GETSaldoClienteDTO(saldoClienteEntity));
        }

        return saldoClienteDTOS;
    }

}
