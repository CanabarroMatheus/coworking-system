package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.ContatoDTO;
import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.ContatoRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    public List<ContatoDTO> trazerTodosContatos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<ContatoEntity>) this.repository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public ContatoDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            Optional<ContatoEntity> contato = this.repository.findById(id);
            if (contato.isPresent()) {
                return new ContatoDTO(contato.get());
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro404(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ContatoDTO salvar(ContatoDTO contatoDTO) throws ErroPersistenciaException {
        try {
            ContatoEntity contato = this.salvarEEditar(contatoDTO.converter());
            return new ContatoDTO(contato);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ContatoDTO editar(ContatoDTO contatoDTO, Integer id) throws ErroPersistenciaException {
        try {
            contatoDTO.setId(id);
            ContatoEntity contato = this.salvarEEditar(contatoDTO.converter());
            return new ContatoDTO(contato);
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private ContatoEntity salvarEEditar(ContatoEntity contatoEntity) {
        return this.repository.save(contatoEntity);
    }

    private List<ContatoDTO> converterListaDTO(List<ContatoEntity> contatos) {
        ArrayList<ContatoDTO> contatosDTO = new ArrayList<>();

        for (ContatoEntity contato : contatos) {
            if (contato.getCliente() != null) {
                contato.getCliente().setContatos(null);
            }
            contatosDTO.add(new ContatoDTO(contato));
        }

        return contatosDTO;
    }
}
