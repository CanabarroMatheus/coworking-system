package br.com.dbccompany.CoworkingSystem.Service;

import br.com.dbccompany.CoworkingSystem.DTO.ClientePacoteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.ContratacaoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETPagamentoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.GET.GETSaldoClienteDTO;
import br.com.dbccompany.CoworkingSystem.DTO.PagamentoDTO;
import br.com.dbccompany.CoworkingSystem.DTO.SaldoClienteDTO;
import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ContratacaoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.SaldoClienteEntity;
import br.com.dbccompany.CoworkingSystem.Exception.ErroPersistenciaException;
import br.com.dbccompany.CoworkingSystem.Exception.RetornoBuscaInvalidoException;
import br.com.dbccompany.CoworkingSystem.Repository.ClientePacoteRepository;
import br.com.dbccompany.CoworkingSystem.Repository.ContratacaoRepository;
import br.com.dbccompany.CoworkingSystem.Repository.PagamentoRepository;
import br.com.dbccompany.CoworkingSystem.Repository.SaldoClienteRepository;
import br.com.dbccompany.CoworkingSystem.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    private ContratacaoService contratacaoService;
    @Autowired
    private ClientePacoteRepository clientePacoteRepository;
    @Autowired
    private SaldoClienteService saldoClienteService;

    public List<GETPagamentoDTO> trazerTodos() throws RetornoBuscaInvalidoException {
        try {
            return this.converterListaDTO((List<PagamentoEntity>) this.pagamentoRepository.findAll());
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    public GETPagamentoDTO encontrarPorId(Integer id) throws RetornoBuscaInvalidoException {
        try {
            Optional<PagamentoEntity> pagamentoEntity = this.pagamentoRepository.findById(id);
            if (pagamentoEntity.isPresent()) {
                return new GETPagamentoDTO(pagamentoEntity.get());
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new RetornoBuscaInvalidoException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public GETPagamentoDTO efetuarPagamento(PagamentoDTO pagamentoDTO) throws ErroPersistenciaException {
        try {
            List<SaldoClienteDTO> saldoClienteDTOS = new ArrayList<>();

            if (!this.verificaPagamentoDuasOperacoes(pagamentoDTO) && !this.verificaPagamentoNenhumaOperacao(pagamentoDTO)) {

                if (pagamentoDTO.getContratacao() != null) {
                    pagamentoDTO.setContratacao(pagamentoDTO.getContratacao() != null
                            ? new ContratacaoDTO(this.contratacaoRepository.findById(pagamentoDTO.getContratacao().getId()).get())
                            : null);

                    PagamentoEntity pagamentoEntity = this.salvarEEditar(pagamentoDTO.converter());
                    GETPagamentoDTO pagamento = new GETPagamentoDTO(pagamentoEntity);
                    pagamento.setContratacao(this.contratacaoService.encontrarPorId(pagamentoDTO.getContratacao().getId()));

                    SaldoClienteEntity saldoClienteEntity = saldoClienteService.gerarSaldoContratacao(pagamentoEntity.getContratacao());
                    saldoClienteDTOS.add(new SaldoClienteDTO(saldoClienteEntity));
                    pagamento.setSaldosGerados(saldoClienteDTOS);

                    return pagamento;
                } else {
                    pagamentoDTO.setClientePacote(pagamentoDTO.getClientePacote() != null
                            ? new ClientePacoteDTO(this.clientePacoteRepository.findById(pagamentoDTO.getClientePacote().getId()).orElse(null))
                            : null);

                    PagamentoEntity pagamentoEntity = this.salvarEEditar(pagamentoDTO.converter());
                    GETPagamentoDTO pagamento = new GETPagamentoDTO(pagamentoEntity);

                    List<SaldoClienteEntity> saldosGerados = saldoClienteService.gerarSaldoClientePacote(pagamentoEntity.getClientePacote());

                    for (SaldoClienteEntity saldoClienteEntity : saldosGerados) {
                        saldoClienteDTOS.add(new SaldoClienteDTO(saldoClienteEntity));
                    }

                    pagamento.setSaldosGerados(saldoClienteDTOS);

                    return pagamento;
                }
            }
            return null;
        } catch (Exception e) {
            LogUtil.enviarErro400(e);
            throw new ErroPersistenciaException();
        }
    }

    private Boolean verificaPagamentoDuasOperacoes(PagamentoDTO pagamentoDTO) {
        return pagamentoDTO.getClientePacote() != null && pagamentoDTO.getContratacao() != null;
    }

    private Boolean verificaPagamentoNenhumaOperacao(PagamentoDTO pagamentoDTO) {
        return pagamentoDTO.getClientePacote() == null && pagamentoDTO.getContratacao() == null;
    }

    private PagamentoEntity salvarEEditar(PagamentoEntity pagamentoEntity) {
        return this.pagamentoRepository.save(pagamentoEntity);
    }

    private List<GETPagamentoDTO> converterListaDTO(List<PagamentoEntity> pagamentoEntities) {
        ArrayList<GETPagamentoDTO> pagamentoDTOS = new ArrayList<>();

        for (PagamentoEntity pagamentoEntity : pagamentoEntities) {
            pagamentoDTOS.add(new GETPagamentoDTO(pagamentoEntity));
        }

        return pagamentoDTOS;
    }

}
