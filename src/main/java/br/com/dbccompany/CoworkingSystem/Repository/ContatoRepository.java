package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    Optional<ContatoEntity> findByValor(String valor);
    List<ContatoEntity> findAllByValorIn(List<String> valores);
}
