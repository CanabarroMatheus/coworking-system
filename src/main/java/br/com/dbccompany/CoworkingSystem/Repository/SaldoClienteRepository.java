package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.Id.SaldoClienteId;
import br.com.dbccompany.CoworkingSystem.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    Optional<SaldoClienteEntity> findByQuantidade(Integer quantidade);
}
