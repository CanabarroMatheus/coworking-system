package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
    Optional<ClientePacoteEntity> findByCliente(ClienteEntity clienteEntity);
    Optional<ClientePacoteEntity> findByPacote(PacoteEntity pacoteEntity);
}
