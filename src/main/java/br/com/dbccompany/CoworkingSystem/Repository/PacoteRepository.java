package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {
    Optional<PacoteEntity> findByValor(Double valor);
}
