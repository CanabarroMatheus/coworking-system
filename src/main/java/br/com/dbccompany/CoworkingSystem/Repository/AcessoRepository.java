package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.AcessoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {
    List<AcessoEntity> findAllBySaldoClienteAndIsEntradaOrderByDataDesc(SaldoClienteEntity saldoClienteEntity, Boolean isEntrada);
}
