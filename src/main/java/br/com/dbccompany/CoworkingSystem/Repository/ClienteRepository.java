package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    List<ClienteEntity> findAllByNome(String nome);
    List<ClienteEntity> findAllByNomeIn(List<String> nomes);
    List<ClienteEntity> findAllByDataNascimento(Date dataNascimento);
    List<ClienteEntity> findAllByDataNascimentoIn(List<Date> datas);
    Optional<ClienteEntity> findByNome(String nome);
    Optional<ClienteEntity> findByCpf(String cpf);
    Optional<ClienteEntity> findByDataNascimento(LocalDate data);
}
