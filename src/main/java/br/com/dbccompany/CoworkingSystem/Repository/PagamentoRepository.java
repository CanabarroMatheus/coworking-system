package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ClientePacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ContratacaoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    Optional<PagamentoEntity> findByClientePacote(ClientePacoteEntity clientePacoteEntity);
    Optional<PagamentoEntity> findByContratacao(ContratacaoEntity contratacaoEntity);
}
