package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ContratacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    List<ContratacaoEntity> findAllByPrazo(Integer prazo);
    Optional<ContratacaoEntity> findByCliente(ClienteEntity clienteEntity);
}
