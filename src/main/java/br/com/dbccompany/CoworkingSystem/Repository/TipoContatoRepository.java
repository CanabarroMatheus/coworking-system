package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {
    Optional<TipoContatoEntity> findByNome(String nome);
    List<TipoContatoEntity> findAllByNomeIn(List<String> nomes);
}
