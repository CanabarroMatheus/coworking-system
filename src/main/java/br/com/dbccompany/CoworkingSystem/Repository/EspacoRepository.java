package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    Optional<EspacoEntity> findByNome(String nome);
}
