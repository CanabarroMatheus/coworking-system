package br.com.dbccompany.CoworkingSystem.Exception;

public class GenericException extends Exception {
    private String message;

    public GenericException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
