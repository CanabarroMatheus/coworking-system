package br.com.dbccompany.CoworkingSystem.Exception;

public class ErroPersistenciaException extends Exception {
    private String message;

    public ErroPersistenciaException(String message) {
        super(message);
    }

    public ErroPersistenciaException() {
        super();
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
