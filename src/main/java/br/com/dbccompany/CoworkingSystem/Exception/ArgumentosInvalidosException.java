package br.com.dbccompany.CoworkingSystem.Exception;

public class ArgumentosInvalidosException extends GenericException{
    public ArgumentosInvalidosException() {
        super("Campos vazios ou nulos na requisição!");
    }
}
