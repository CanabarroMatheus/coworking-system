package br.com.dbccompany.CoworkingSystem.Exception;

public class ErroNaOperacaoException extends Exception {
    private String message;

    public ErroNaOperacaoException(String message) {
        super(message);
    }

    public ErroNaOperacaoException() {
        super();
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
