package br.com.dbccompany.CoworkingSystem.Exception;

public class RetornoBuscaInvalidoException extends Exception {
    private String message;

    public RetornoBuscaInvalidoException(String message) {
        super(message);
    }

    public RetornoBuscaInvalidoException() {
        super();
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
