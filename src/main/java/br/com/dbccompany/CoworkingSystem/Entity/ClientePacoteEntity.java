package br.com.dbccompany.CoworkingSystem.Entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Clientes_Pacotes")
public class ClientePacoteEntity {

    @Id
    @SequenceGenerator(name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private Integer quantidade;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private ClienteEntity cliente;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private PacoteEntity pacote;

    @OneToMany(mappedBy = "clientePacote", cascade = CascadeType.ALL)
    private List<PagamentoEntity> pagamento;

    public ClientePacoteEntity() {
    }

    public ClientePacoteEntity(Integer id, ClienteEntity cliente, PacoteEntity pacote, Integer quantidade, List<PagamentoEntity> pagamento) {
        this.id = id;
        this.cliente = cliente;
        this.pacote = pacote;
        this.quantidade = quantidade;
        this.pagamento = pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoEntity> pagamento) {
        this.pagamento = pagamento;
    }
}
