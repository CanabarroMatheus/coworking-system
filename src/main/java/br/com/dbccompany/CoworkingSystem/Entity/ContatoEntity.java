package br.com.dbccompany.CoworkingSystem.Entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity(name = "Contato")
public class ContatoEntity {

    @Id
    @SequenceGenerator(name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String valor;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_Tipo_contato")
    private TipoContatoEntity tipoContato;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_Cliente")
    private ClienteEntity cliente;

    public ContatoEntity() {
    }

    public ContatoEntity(Integer id, String valor, TipoContatoEntity tipoContato, ClienteEntity cliente) {
        this.id = id;
        this.valor = valor;
        this.tipoContato = tipoContato;
        this.cliente = cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
