package br.com.dbccompany.CoworkingSystem.Entity.Enum;

public enum TipoContratacaoEnum {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES, ESPECIAL
}
