package br.com.dbccompany.CoworkingSystem.Entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Pacotes")
public class PacoteEntity {

    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private Double valor;

    @OneToMany(cascade = CascadeType.ALL)
    private List<EspacoPacoteEntity> espacoPacote;

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacoteEntity> pacoteCliente;

    public PacoteEntity() {
    }

    public PacoteEntity(Integer id, Double valor, List<EspacoPacoteEntity> espacoPacote, List<ClientePacoteEntity> pacoteCliente) {
        this.id = id;
        this.valor = valor;
        this.espacoPacote = espacoPacote;
        this.pacoteCliente = pacoteCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<EspacoPacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<ClientePacoteEntity> getPacoteCliente() {
        return pacoteCliente;
    }

    public void setPacoteCliente(List<ClientePacoteEntity> pacoteCliente) {
        this.pacoteCliente = pacoteCliente;
    }
}
