package br.com.dbccompany.CoworkingSystem.Entity;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.Id.SaldoClienteId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity(name = "Saldo_cliente")
public class SaldoClienteEntity {

    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_contratacao", nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private LocalDate vencimento;

    @ManyToOne
    @MapsId("id_espaco")
    private EspacoEntity espaco;

    @ManyToOne
    @MapsId("id_cliente")
    private ClienteEntity cliente;

    @OneToMany(mappedBy = "saldoCliente")
    private List<AcessoEntity> acesso;

    public SaldoClienteEntity() {
    }

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public List<AcessoEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessoEntity> acesso) {
        this.acesso = acesso;
    }
}
