package br.com.dbccompany.CoworkingSystem.Entity.Enum;

public enum TipoPagamentoEnum {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
