package br.com.dbccompany.CoworkingSystem.Entity;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "Espacos_Pacotes")
public class EspacoPacoteEntity {

    @Id
    @SequenceGenerator(name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_contratacao", nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private EspacoEntity espaco;

    public EspacoPacoteEntity() {
    }

    public EspacoPacoteEntity(Integer id, TipoContratacaoEnum tipoContratacao, Integer quantidade, Integer prazo, EspacoEntity id_espaco, PacoteEntity id_pacote) {
        this.id = id;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.prazo = prazo;
        this.espaco = id_espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }
}
