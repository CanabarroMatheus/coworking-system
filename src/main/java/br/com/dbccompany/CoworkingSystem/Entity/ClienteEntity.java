package br.com.dbccompany.CoworkingSystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity(name = "clientes")
public class ClienteEntity {

    @Id
    @SequenceGenerator(name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true)
    private Character[] cpf = new Character[11];

    @Column(nullable = false)
    private LocalDate dataNascimento;

    @OneToMany(cascade = CascadeType.ALL)
    @Column(nullable = false)
    private List<ContatoEntity> contatos;

    @OneToMany
    private List<SaldoClienteEntity> saldoCliente;

    @OneToMany
    private List<ClientePacoteEntity> clientePacote;

    @OneToMany
    private List<ContratacaoEntity> contratacao;

    public ClienteEntity() {
    }

    public ClienteEntity(Integer id, String nome, Character[] cpf, LocalDate dataNascimento, List<ContatoEntity> contatos, List<SaldoClienteEntity> saldoCliente, List<ClientePacoteEntity> clientePacote, List<ContratacaoEntity> contratacao) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.contatos = contatos;
        this.saldoCliente = saldoCliente;
        this.clientePacote = clientePacote;
        this.contratacao = contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character[] getCpf() {
        return cpf;
    }

    public void setCpf(Character[] cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<ClientePacoteEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
