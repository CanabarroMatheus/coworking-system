package br.com.dbccompany.CoworkingSystem.Entity;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoPagamentoEnum;

import javax.persistence.*;

@Entity(name = "Pagamentos")
public class PagamentoEntity {

    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pagamento", nullable = false)
    private TipoPagamentoEnum tipoPagamento;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private ClientePacoteEntity clientePacote;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private ContratacaoEntity contratacao;

    public PagamentoEntity() {
    }

    public PagamentoEntity(Integer id, TipoPagamentoEnum tipoPagamento, ClientePacoteEntity clientePacote, ContratacaoEntity contratacao) {
        this.id = id;
        this.tipoPagamento = tipoPagamento;
        this.clientePacote = clientePacote;
        this.contratacao = contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

}
