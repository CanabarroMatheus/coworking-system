package br.com.dbccompany.CoworkingSystem.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "Acessos")
public class AcessoEntity {

    @Id
    @SequenceGenerator(name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private Boolean isEntrada;
    private LocalDateTime data;
    private Boolean excessao;

    @ManyToOne
    private SaldoClienteEntity saldoCliente;

    public AcessoEntity() {
    }

    public AcessoEntity(Integer id, Boolean isEntrada, LocalDateTime data, Boolean excessao, SaldoClienteEntity saldoCliente) {
        this.id = id;
        this.isEntrada = isEntrada;
        this.data = data;
        this.excessao = excessao;
        this.saldoCliente = saldoCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcessao() {
        return excessao;
    }

    public void setExcessao(Boolean excessao) {
        this.excessao = excessao;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
