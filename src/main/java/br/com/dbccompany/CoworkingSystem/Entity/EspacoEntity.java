package br.com.dbccompany.CoworkingSystem.Entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Espacos")
public class EspacoEntity {

    @Id
    @SequenceGenerator(name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(name = "qtdPessoas", nullable = false)
    private Integer quantidadePessoas;

    @Column(nullable = false)
    private Double valor;

    @OneToMany
    private List<ContratacaoEntity> contratacao;

    public EspacoEntity() {
    }

    public EspacoEntity(Integer id, String nome, Integer quantidadePessoas, Double valor, List<SaldoClienteEntity> saldoCliente, List<EspacoPacoteEntity> espacoPacote, List<ContratacaoEntity> contratacao) {
        this.id = id;
        this.nome = nome;
        this.quantidadePessoas = quantidadePessoas;
        this.valor = valor;
        this.contratacao = contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Integer quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
