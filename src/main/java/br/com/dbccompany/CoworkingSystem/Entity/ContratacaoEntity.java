package br.com.dbccompany.CoworkingSystem.Entity;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "Contratacao")
public class ContratacaoEntity {

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_contratacao", nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    private Double desconto;

    @Column(nullable = false)
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private ClienteEntity cliente;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private EspacoEntity espaco;

    @OneToMany(mappedBy = "contratacao", fetch = FetchType.LAZY)
    private List<PagamentoEntity> pagamento;

    public ContratacaoEntity() {
    }

    public ContratacaoEntity(Integer id, TipoContratacaoEnum tipoContratacao, Integer quantidade, Double desconto, Integer prazo, ClienteEntity cliente, EspacoEntity espaco, List<PagamentoEntity> pagamento) {
        this.id = id;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.desconto = desconto;
        this.prazo = prazo;
        this.cliente = cliente;
        this.espaco = espaco;
        this.pagamento = pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoEntity> pagamento) {
        this.pagamento = pagamento;
    }
}
