package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.*;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoPagamentoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PagamentoRepositoryTest {

    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private ClientePacoteRepository cliente_x_pacoteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Test
    public void salvarPagamentoClientePacote(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setValor("51995544835");
        cont2.setValor("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQuantidadePessoas(500000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Paris");
        espaco2.setValor(22.0);
        espaco2.setQuantidadePessoas(200000);
        espacoRepository.save(espaco);
        espacoRepository.save(espaco2);
        EspacoPacoteEntity ep = new EspacoPacoteEntity();
        ep.setEspaco(espaco);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        ep.setQuantidade(30);
        ep.setPrazo(60);
        EspacoPacoteEntity ep2 = new EspacoPacoteEntity();
        ep2.setEspaco(espaco2);
        ep2.setQuantidade(20);
        ep2.setTipoContratacao(TipoContratacaoEnum.HORAS);
        ep2.setPrazo(10);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(ep);
        espacos.add(ep2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacote(espacos);
        pacoteRepository.save(pacote);

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacote);
        clientePacote.setCliente(cliente);
        clientePacote.setQuantidade(5);
        cliente_x_pacoteRepository.save(clientePacote);

        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setClientePacote(clientePacote);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DINHEIRO);
        PagamentoEntity salvo = repository.save(pagamento);
        assertEquals(pagamento.getClientePacote().getCliente().getCpf(), repository.findById(salvo.getId()).get().getClientePacote().getCliente().getCpf());
    }

    @Test
    public void pagarContratacao(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setValor("51995544835");
        cont2.setValor("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQuantidadePessoas(500000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Paris");
        espaco2.setValor(22.0);
        espaco2.setQuantidadePessoas(200000);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(cliente);
        contratacao.setEspaco(espaco);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        contratacao.setQuantidade(30);
        contratacao.setPrazo(90);
        contratacao.setDesconto(0.15);
        contratacaoRepository.save(contratacao);

        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setContratacao(contratacao);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);
        assertEquals(pagamento.getContratacao(), repository.findByContratacao(pagamento.getContratacao()).get().getContratacao());
    }

    @Test
    public void editarPagamento(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setValor("51995544835");
        cont2.setValor("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQuantidadePessoas(500000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Paris");
        espaco2.setValor(22.0);
        espaco2.setQuantidadePessoas(200000);
        espacoRepository.save(espaco);
        espacoRepository.save(espaco2);
        EspacoPacoteEntity ep = new EspacoPacoteEntity();
        ep.setEspaco(espaco);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        ep.setQuantidade(30);
        ep.setPrazo(60);
        EspacoPacoteEntity ep2 = new EspacoPacoteEntity();
        ep2.setEspaco(espaco2);
        ep2.setQuantidade(20);
        ep2.setTipoContratacao(TipoContratacaoEnum.HORAS);
        ep2.setPrazo(10);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(ep);
        espacos.add(ep2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacote(espacos);
        pacoteRepository.save(pacote);

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacote);
        clientePacote.setCliente(cliente);
        clientePacote.setQuantidade(5);
        cliente_x_pacoteRepository.save(clientePacote);

        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setClientePacote(clientePacote);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DINHEIRO);
        PagamentoEntity salvo = repository.save(pagamento);
        pagamento.setTipoPagamento(TipoPagamentoEnum.CREDITO);
        pagamento.setId(salvo.getId());
        repository.save(pagamento);
        assertEquals(pagamento.getTipoPagamento(), repository.findByClientePacote(pagamento.getClientePacote()).get().getTipoPagamento());
    }
}
