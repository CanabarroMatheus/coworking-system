package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ClienteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void criarCliente(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("email");
        tp2.setNome("telefone");
        this.tipoContatoRepository.save(tp);
        this.tipoContatoRepository.save(tp2);
        ContatoEntity cont1 = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont1.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont1.setValor("paula@gmail.com");
        cont2.setValor("5551996543451");
        ArrayList<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont1);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        Character[] cpf = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1'};
        cliente.setCpf(cpf);
        this.repository.save(cliente);
        assertEquals(cliente.getNome(), this.repository.findByNome(cliente.getNome()).get().getNome());
    }

    @Test
    public void editarCliente(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("email");
        tp2.setNome("telefone");
        this.tipoContatoRepository.save(tp);
        this.tipoContatoRepository.save(tp2);
        ContatoEntity cont1 = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont1.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont1.setValor("rodrigao@gmail.com");
        cont2.setValor("5551999999999");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont1);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Rodrigo");
        cliente.setDataNascimento(LocalDate.parse("2005-10-30"));
        Character[] cpf = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1'};
        cliente.setCpf(cpf);
        ClienteEntity salvo = this.repository.save(cliente);
        cliente.setId(2);
        cliente.setDataNascimento(LocalDate.parse("2000-12-23"));
        this.repository.save(cliente);
        assertEquals(cliente.getDataNascimento(), this.repository.findByDataNascimento(cliente.getDataNascimento()).get().getDataNascimento());
    }
}
