package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.ContatoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DataJpaTest
public class ContatoRepositoryTest {
    @Autowired
    private ContatoRepository repository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarContato(){
        TipoContatoEntity tp = new TipoContatoEntity();
        tp.setNome("telefone");
        this.tipoContatoRepository.save(tp);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(tp);
        contato.setValor("51986867525");
        this.repository.save(contato);
        assertEquals(contato.getValor(), this.repository.findById(1).get().getValor());
    }

    @Test
    public void editarContato(){
        TipoContatoEntity tp = new TipoContatoEntity();
        tp.setNome("telefone");
        this.tipoContatoRepository.save(tp);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(tp);
        contato.setValor("51986867525");
        this.repository.save(contato);
        contato.setValor("52576868915");
        contato.setId(2);
        ContatoEntity salvo = this.repository.save(contato);
        assertEquals(contato.getValor(), this.repository.findByValor(salvo.getValor()).get().getValor());
        List<ContatoEntity> contatos = (List<ContatoEntity>) this.repository.findAll();
        assertEquals(1, contatos.size());
    }
}
