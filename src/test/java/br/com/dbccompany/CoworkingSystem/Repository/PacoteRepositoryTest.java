package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;
import br.com.dbccompany.CoworkingSystem.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class PacoteRepositoryTest {

    @Autowired
    private PacoteRepository repository;

    @Autowired EspacoRepository espacoRepository;

    @Test
    public void salvarPacote(){
        EspacoEntity espaco1 = new EspacoEntity();
        espaco1.setValor(1.2);
        espaco1.setNome("Praia");
        espaco1.setQuantidadePessoas(2000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setValor(1.2);
        espaco2.setNome("Cidade");
        espaco2.setQuantidadePessoas(2000);
        this.espacoRepository.save(espaco2);
        this.espacoRepository.save(espaco2);
        EspacoPacoteEntity espacoPacote1 = new EspacoPacoteEntity();
        espacoPacote1.setEspaco(espaco2);
        espacoPacote1.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        espacoPacote1.setQuantidade(30);
        espacoPacote1.setPrazo(60);
        EspacoPacoteEntity espacoPacote2 = new EspacoPacoteEntity();
        espacoPacote2.setEspaco(espaco2);
        espacoPacote2.setQuantidade(20);
        espacoPacote2.setTipoContratacao(TipoContratacaoEnum.HORAS);
        espacoPacote2.setPrazo(10);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(espacoPacote1);
        espacos.add(espacoPacote2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacote(espacos);
        this.repository.save(pacote);
        assertEquals(pacote.getValor(), repository.findByValor(pacote.getValor()).get().getValor());
    }

    @Test
    public void editarEspaco(){
        EspacoEntity espaco1 = new EspacoEntity();
        espaco1.setValor(1.2);
        espaco1.setNome("Serra");
        espaco1.setQuantidadePessoas(2000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setValor(1.2);
        espaco2.setNome("Montanha");
        espaco2.setQuantidadePessoas(2000);
        this.espacoRepository.save(espaco1);
        this.espacoRepository.save(espaco2);
        EspacoPacoteEntity espacoPacote1 = new EspacoPacoteEntity();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        espacoPacote1.setQuantidade(30);
        espacoPacote1.setPrazo(60);
        EspacoPacoteEntity espacoPacote2 = new EspacoPacoteEntity();
        espacoPacote2.setEspaco(espaco2);
        espacoPacote2.setQuantidade(20);
        espacoPacote2.setTipoContratacao(TipoContratacaoEnum.HORAS);
        espacoPacote2.setPrazo(10);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(espacoPacote1);
        espacos.add(espacoPacote2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacote(espacos);
        PacoteEntity salvo = this.repository.save(pacote);
        pacote.setValor(500.0);
        pacote.setId(salvo.getId());
        this.repository.save(pacote);
        assertEquals(pacote.getValor(), this.repository.findById(salvo.getId()).get().getValor());
    }
}
