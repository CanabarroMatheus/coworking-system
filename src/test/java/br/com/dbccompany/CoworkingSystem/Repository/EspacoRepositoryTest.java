package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EspacoRepositoryTest {

    @Autowired
    private EspacoRepository repository;

    @Test
    public void salvarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setValor(0.2);
        espaco.setNome("Escritório");
        espaco.setQuantidadePessoas(100);
        this.repository.save(espaco);
        assertEquals(espaco.getNome(), this.repository.findByNome(espaco.getNome()).get().getNome());
    }

    @Test
    public void editarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setValor(0.8);
        espaco.setNome("Sala de reunião");
        espaco.setQuantidadePessoas(15);
        this.repository.save(espaco);
        espaco.setId(2);
        espaco.setNome("Cantina");
        espaco.setQuantidadePessoas(30);
        this.repository.save(espaco);
        List<EspacoEntity> espacoEntities = (List<EspacoEntity>) this.repository.findAll();
        assertEquals(1, espacoEntities.size());
        assertEquals(espaco.getNome(), repository.findByNome(espaco.getNome()).get().getNome());
    }

}
