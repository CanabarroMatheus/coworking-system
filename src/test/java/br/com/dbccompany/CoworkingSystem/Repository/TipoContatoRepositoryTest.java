package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salvarTipoContato(){
        TipoContatoEntity tp = new TipoContatoEntity();
        tp.setNome("telefone");
        this.repository.save(tp);
        assertEquals(tp.getNome(), this.repository.findByNome(tp.getNome()).get().getNome());
    }

    @Test
    public void editarTipoContato(){
        TipoContatoEntity tp = new TipoContatoEntity();
        tp.setNome("telefone");
        this.repository.save(tp);
        TipoContatoEntity novoTp = new TipoContatoEntity();
        novoTp.setNome("email");
        novoTp.setId(1);
        this.repository.save(novoTp);
        assertEquals(novoTp.getNome(), this.repository.findById(1).get().getNome());
    }

    @Test
    public void buscarTodosItens(){
        TipoContatoEntity tp1 = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp1.setNome("telegram");
        tp2.setNome("msn");
        this.repository.save(tp1);
        this.repository.save(tp2);
        ArrayList<TipoContatoEntity> expected = new ArrayList<>();
        expected.add(tp1);
        expected.add(tp2);
        assertEquals(expected, this.repository.findAll());
    }
}
