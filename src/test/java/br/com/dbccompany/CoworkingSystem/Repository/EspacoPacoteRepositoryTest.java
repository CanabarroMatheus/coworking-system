package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoEntity;
import br.com.dbccompany.CoworkingSystem.Entity.EspacoPacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EspacoPacoteRepositoryTest {


    @Autowired
    private EspacoPacoteRepository repository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Test
    public void salvarEspacoPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Praia");
        espaco.setQuantidadePessoas(1000);
        espaco.setValor(1.0);
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPrazo(30);
        espacoPacote.setQuantidade(30);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        EspacoPacoteEntity salvo = this.repository.save(espacoPacote);
        assertEquals(espacoPacote.getPrazo(), this.repository.findById(salvo.getId()).get().getPrazo());
    }

    @Test
    public void editarEspacoPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Praia");
        espaco.setQuantidadePessoas(1000);
        espaco.setValor(1.0);
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPrazo(30);
        espacoPacote.setQuantidade(30);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        EspacoPacoteEntity salvo = this.repository.save(espacoPacote);
        espacoPacote.setQuantidade(1000);
        espacoPacote.setId(salvo.getId());
        EspacoPacoteEntity editado = this.repository.save(espacoPacote);
        assertEquals(espacoPacote.getQuantidade(), this.repository.findById(salvo.getId()).get().getQuantidade());
    }

}
