package br.com.dbccompany.CoworkingSystem.Repository;

import br.com.dbccompany.CoworkingSystem.Entity.*;
import br.com.dbccompany.CoworkingSystem.Entity.Enum.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ClientePacoteRepositoryTest {

    @Autowired
    private ClientePacoteRepository repository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarClientePacote(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setValor("51995544835");
        cont2.setValor("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQuantidadePessoas(500000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Paris");
        espaco2.setValor(22.0);
        espaco2.setQuantidadePessoas(200000);
        espacoRepository.save(espaco);
        espacoRepository.save(espaco2);
        EspacoPacoteEntity ep = new EspacoPacoteEntity();
        ep.setEspaco(espaco);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        ep.setQuantidade(30);
        ep.setPrazo(60);
        EspacoPacoteEntity ep2 = new EspacoPacoteEntity();
        ep2.setEspaco(espaco2);
        ep2.setQuantidade(20);
        ep2.setTipoContratacao(TipoContratacaoEnum.HORAS);
        ep2.setPrazo(10);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(ep);
        espacos.add(ep2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacote(espacos);
        pacoteRepository.save(pacote);

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacote);
        clientePacote.setCliente(cliente);
        clientePacote.setQuantidade(5);
        ClientePacoteEntity salvo = repository.save(clientePacote);
        assertEquals(clientePacote.getCliente().getDataNascimento(), repository.findByCliente(clientePacote.getCliente()).get().getCliente().getDataNascimento());
        assertEquals(clientePacote.getPacote().getValor(), repository.findByPacote(clientePacote.getPacote()).get().getPacote().getValor());
    }

    @Test
    public void editarClientePacote(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setValor("51995544835");
        cont2.setValor("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQuantidadePessoas(500000);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Paris");
        espaco2.setValor(22.0);
        espaco2.setQuantidadePessoas(200000);
        espacoRepository.save(espaco);
        espacoRepository.save(espaco2);
        EspacoPacoteEntity ep = new EspacoPacoteEntity();
        ep.setEspaco(espaco);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIAS);
        ep.setQuantidade(30);
        ep.setPrazo(60);
        EspacoPacoteEntity ep2 = new EspacoPacoteEntity();
        ep2.setEspaco(espaco2);
        ep2.setQuantidade(20);
        ep2.setTipoContratacao(TipoContratacaoEnum.HORAS);
        ep2.setPrazo(10);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(ep);
        espacos.add(ep2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacote(espacos);
        pacoteRepository.save(pacote);

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacote);
        clientePacote.setCliente(cliente);
        clientePacote.setQuantidade(5);
        ClientePacoteEntity salvo = repository.save(clientePacote);
        clientePacote.setQuantidade(30000);
        clientePacote.setId(salvo.getId());
        assertEquals(clientePacote.getQuantidade(), repository.findByPacote(clientePacote.getPacote()).get().getQuantidade());

    }
}
